module.exports = {
  '/wroom': {
    'target': 'https://api.bezpiecznywozek.com',
    'secure': false,
    'changeOrigin': true,
    'logLevel': 'debug',
    'bypass': function (req) {
      req.headers['custom-header'] = 'custom value';
    },
    'pathRewrite': {
      '^/wroom': '/'
    }
  }
};
