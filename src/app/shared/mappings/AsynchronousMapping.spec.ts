import {AsynchronousMapping} from '@app/shared/mappings/AsynchronousMapping';
import {Mapping} from '@app/shared/mappings/Mapping';
import {BehaviorSubject, Observable} from 'rxjs';

describe('AsynchronousMapping', () => {
  class MockMapping extends AsynchronousMapping<string, string> {
    protected map(source: string): Observable<string> {
      return new BehaviorSubject(source.toUpperCase());
    }
  }
  const sut: Mapping<string, string> = new MockMapping();

  it('maps null to null', (done: DoneFn) => {
    const result: Observable<string> = sut.convert(null);

    result.subscribe((value) => {
      expect(value).toBe(null);
      done();
    });
  });

  it('maps undefined to null', (done: DoneFn) => {
    const result: Observable<string> = sut.convert(undefined);

    result.subscribe((value) => {
      expect(value).toBe(null);
      done();
    });
  });

  it("maps '' to ''", (done: DoneFn) => {
    const result: Observable<string> = sut.convert('');

    result.subscribe((value) => {
      expect(value).toBe('');
      done();
    });
  });

  it("maps 'Test' to 'TEST'", (done: DoneFn) => {
    const result: Observable<string> = sut.convert('Test');

    result.subscribe((value) => {
      expect(value).toBe('TEST');
      done();
    });
  });
});
