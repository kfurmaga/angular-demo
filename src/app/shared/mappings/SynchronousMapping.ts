import {Mapping} from '@app/shared/mappings/Mapping';
import {BehaviorSubject, Observable} from 'rxjs';

export abstract class SynchronousMapping<TFrom, TTo> implements Mapping<TFrom, TTo> {
  public convert = (source: TFrom): Observable<TTo> => {
    const result: TTo = source === null || source === undefined ? null : this.map(source);
    return new BehaviorSubject(result);
  };

  protected abstract map(source: TFrom): TTo;
}
