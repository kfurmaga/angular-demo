import {Mapping} from '@app/shared/mappings/Mapping';
import {AsyncSubject, Observable} from 'rxjs';

export abstract class AsynchronousMapping<TFrom, TTo> implements Mapping<TFrom, TTo> {
  public convert = (source: TFrom): Observable<TTo> => {
    if (source === null || source === undefined) {
      const subject: AsyncSubject<TTo> = new AsyncSubject<TTo>();
      subject.next(null);
      subject.complete();
      return subject;
    }

    return this.map(source);
  };

  protected abstract map(source: TFrom): Observable<TTo>;
}
