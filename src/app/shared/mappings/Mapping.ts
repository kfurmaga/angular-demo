import {Observable} from 'rxjs';

export interface Mapping<TFrom, TTo> {
  convert(source: TFrom): Observable<TTo>;
}
