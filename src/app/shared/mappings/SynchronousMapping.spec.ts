import {Mapping} from '@app/shared/mappings/Mapping';
import {SynchronousMapping} from '@app/shared/mappings/SynchronousMapping';
import {Observable} from 'rxjs';

describe('SynchronousMapping', () => {
  class MockMapping extends SynchronousMapping<string, string> {
    protected map(source: string): string {
      return source.toUpperCase();
    }
  }
  const sut: Mapping<string, string> = new MockMapping();

  it('maps null to null', (done: DoneFn) => {
    const result: Observable<string> = sut.convert(null);

    result.subscribe((value) => {
      expect(value).toBe(null);
      done();
    });
  });

  it('maps undefined to null', (done: DoneFn) => {
    const result: Observable<string> = sut.convert(undefined);

    result.subscribe((value) => {
      expect(value).toBe(null);
      done();
    });
  });

  it("maps '' to ''", (done: DoneFn) => {
    const result: Observable<string> = sut.convert('');

    result.subscribe((value) => {
      expect(value).toBe('');
      done();
    });
  });

  it("maps 'Test' to 'TEST'", (done: DoneFn) => {
    const result: Observable<string> = sut.convert('Test');

    result.subscribe((value) => {
      expect(value).toBe('TEST');
      done();
    });
  });
});
