import {Message} from '@app/shared/model/Message';
import {every} from '@app/shared/model/observable/mergeMap';
import {BehaviorSubject, Observable} from 'rxjs';

describe('Observable.mergeMap.every', () => {
  const itemToResult: (value: string) => Observable<string> = (value: string) => new BehaviorSubject(value.toUpperCase());

  it('throws error when null is passed as itemToResult', () => {
    expect(() => {
      every(null);
    }).toThrowError(Message.mustBeFunction('Observable.mergeMap.every', 'itemToResult'));
  });

  it('throws error when undefined', () => {
    expect(() => {
      every(undefined);
    }).toThrowError(Message.mustBeFunction('Observable.mergeMap.every', 'itemToResult'));
  });

  it("returns function which returns observable containing array with all values mapped with itemToResult function.", (done: DoneFn) => {
    const list: string[] = ['a', 'b', 'c', 'd', 'e'];
    const sut: (item: string[]) => Observable<string[]> = every(itemToResult);
    const result: Observable<string[]> = sut(list);

    result.subscribe((items) => {
      expect(items).toEqual(['A', 'B', 'C', 'D', 'E']);
      done();
    });
  });

  it("returns function which returns observable containing empty array when empty array is passed as parameter.", (done: DoneFn) => {
    const list: string[] = [];
    const sut: (item: string[]) => Observable<string[]> = every(itemToResult);
    const result: Observable<string[]> = sut(list);

    result.subscribe((items) => {
      expect(items).toEqual([]);
      done();
    });
  });

  it("returns function which throws error when null is passed as parameter.", () => {
    const list: string[] = [];
    const sut: (items: string[]) => Observable<string[]> = every(itemToResult);

    expect(() => {
      sut(null);
    }).toThrowError(Message.canNotBeNull('Observable.mergeMap.every.itemsToResults', 'items'));
  });

  it("returns function which throws error when undefined is passed as parameter.", () => {
    const list: string[] = [];
    const sut: (items: string[]) => Observable<string[]> = every(itemToResult);

    expect(() => {
      sut(undefined);
    }).toThrowError(Message.canNotBeUndefined('Observable.mergeMap.every.itemsToResults', 'items'));
  });
});
