import {Argument} from '@app/shared/model/Argument';
import {combineLatest, BehaviorSubject, Observable} from 'rxjs';

export function every<TItem, TResult>(
  itemToResult: (item: TItem) => Observable<TResult>
): (item: TItem[]) => Observable<TResult[]> {
  Argument.mustBeFunction(itemToResult, 'Observable.mergeMap.every', 'itemToResult');

  return (items: TItem[]): Observable<TResult[]> => {
    Argument.mustBeDefined(items, 'Observable.mergeMap.every.itemsToResults', 'items');

    return items.length > 0 ? combineLatest(items.map(itemToResult)) : new BehaviorSubject([]);
  };
}
