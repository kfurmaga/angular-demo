import {Argument} from '@app/shared/model/Argument';

describe('Argument', () => {
  it('throws error when throwValidationError was executed.', () => {
    expect(() => {
      Argument.throwValidationError('method', 'argument', "because throw that's why");
    }).toThrow();
  });

  it('throws error when canNotBeFalsy gets falsy value.', () => {
    expect(() => Argument.canNotBeFalsy(false, 'method', 'argument')).toThrow();
    expect(() => Argument.canNotBeFalsy('', 'method', 'argument')).toThrow();
    expect(() => Argument.canNotBeFalsy(0, 'method', 'argument')).toThrow();
    expect(() => Argument.canNotBeFalsy(null, 'method', 'argument')).toThrow();
    expect(() => Argument.canNotBeFalsy(undefined, 'method', 'argument')).toThrow();
    expect(() => Argument.canNotBeFalsy(NaN, 'method', 'argument')).toThrow();
  });

  it("doesn't throw error when canNotBeFalsy gets truthy value.", () => {
    expect(() => Argument.canNotBeFalsy(true, 'method', 'argument')).not.toThrow();
    expect(() => Argument.canNotBeFalsy('test', 'method', 'argument')).not.toThrow();
    expect(() => Argument.canNotBeFalsy(1, 'method', 'argument')).not.toThrow();
    expect(() => Argument.canNotBeFalsy({}, 'method', 'argument')).not.toThrow();
    expect(() => Argument.canNotBeFalsy([], 'method', 'argument')).not.toThrow();
    expect(() => Argument.canNotBeFalsy([0, 1, 2], 'method', 'argument')).not.toThrow();
    expect(() => Argument.canNotBeFalsy(() => {}, 'method', 'argument')).not.toThrow();
  });

  it('throws error when mustBe gets false as assertionResult.', () => {
    const value: number = 2;
    expect(() => Argument.mustBe('higher than 3', value > 3, 'method', 'argument')).toThrow();
  });

  it("doesn't throw error when mustBe gets true as assertionResult.", () => {
    const value: number = 6;
    expect(() => Argument.mustBe('higher than 3', value > 3, 'method', 'argument')).not.toThrow();
  });

  it('throws error when canNotBeNull gets null value.', () => {
    expect(() => Argument.canNotBeNull(null, 'method', 'argument')).toThrow();
  });

  it("doesn't throw error when canNotBeNull gets not null value.", () => {
    expect(() => Argument.canNotBeNull(true, 'method', 'argument')).not.toThrow();
    expect(() => Argument.canNotBeNull('test', 'method', 'argument')).not.toThrow();
    expect(() => Argument.canNotBeNull(1, 'method', 'argument')).not.toThrow();
    expect(() => Argument.canNotBeNull({}, 'method', 'argument')).not.toThrow();
    expect(() => Argument.canNotBeNull([], 'method', 'argument')).not.toThrow();
    expect(() => Argument.canNotBeNull([0, 1, 2], 'method', 'argument')).not.toThrow();
    expect(() => Argument.canNotBeNull(() => {}, 'method', 'argument')).not.toThrow();
    expect(() => Argument.canNotBeNull(false, 'method', 'argument')).not.toThrow();
    expect(() => Argument.canNotBeNull('', 'method', 'argument')).not.toThrow();
    expect(() => Argument.canNotBeNull(0, 'method', 'argument')).not.toThrow();
    expect(() => Argument.canNotBeNull(undefined, 'method', 'argument')).not.toThrow();
    expect(() => Argument.canNotBeNull(NaN, 'method', 'argument')).not.toThrow();
  });

  it('throws error when canNotBeUndefined gets undefined value.', () => {
    expect(() => Argument.canNotBeUndefined(undefined, 'method', 'argument')).toThrow();
  });

  it("doesn't throw error when canNotBeNull gets not undefined value.", () => {
    expect(() => Argument.canNotBeUndefined(true, 'method', 'argument')).not.toThrow();
    expect(() => Argument.canNotBeUndefined('test', 'method', 'argument')).not.toThrow();
    expect(() => Argument.canNotBeUndefined(1, 'method', 'argument')).not.toThrow();
    expect(() => Argument.canNotBeUndefined({}, 'method', 'argument')).not.toThrow();
    expect(() => Argument.canNotBeUndefined([], 'method', 'argument')).not.toThrow();
    expect(() => Argument.canNotBeUndefined([0, 1, 2], 'method', 'argument')).not.toThrow();
    expect(() => Argument.canNotBeUndefined(() => {}, 'method', 'argument')).not.toThrow();
    expect(() => Argument.canNotBeUndefined(false, 'method', 'argument')).not.toThrow();
    expect(() => Argument.canNotBeUndefined('', 'method', 'argument')).not.toThrow();
    expect(() => Argument.canNotBeUndefined(0, 'method', 'argument')).not.toThrow();
    expect(() => Argument.canNotBeUndefined(null, 'method', 'argument')).not.toThrow();
    expect(() => Argument.canNotBeUndefined(NaN, 'method', 'argument')).not.toThrow();
  });

  it('throws error when mustBeDefined gets undefined value.', () => {
    expect(() => Argument.mustBeDefined(undefined, 'method', 'argument')).toThrow();
  });

  it('throws error when mustBeDefined gets null value.', () => {
    expect(() => Argument.mustBeDefined(null, 'method', 'argument')).toThrow();
  });

  it("doesn't throw error when canNotBeNull gets not undefined value.", () => {
    expect(() => Argument.mustBeDefined(true, 'method', 'argument')).not.toThrow();
    expect(() => Argument.mustBeDefined('test', 'method', 'argument')).not.toThrow();
    expect(() => Argument.mustBeDefined(1, 'method', 'argument')).not.toThrow();
    expect(() => Argument.mustBeDefined({}, 'method', 'argument')).not.toThrow();
    expect(() => Argument.mustBeDefined([], 'method', 'argument')).not.toThrow();
    expect(() => Argument.mustBeDefined([0, 1, 2], 'method', 'argument')).not.toThrow();
    expect(() => Argument.mustBeDefined(() => {}, 'method', 'argument')).not.toThrow();
    expect(() => Argument.mustBeDefined(false, 'method', 'argument')).not.toThrow();
    expect(() => Argument.mustBeDefined('', 'method', 'argument')).not.toThrow();
    expect(() => Argument.mustBeDefined(0, 'method', 'argument')).not.toThrow();
    expect(() => Argument.mustBeDefined(NaN, 'method', 'argument')).not.toThrow();
  });

  it('throws error when mustBeAnObject gets non object value.', () => {
    expect(() => Argument.mustBeAnObject(null, 'method', 'argument')).toThrow();
    expect(() => Argument.mustBeAnObject(true, 'method', 'argument')).toThrow();
    expect(() => Argument.mustBeAnObject('test', 'method', 'argument')).toThrow();
    expect(() => Argument.mustBeAnObject(1, 'method', 'argument')).toThrow();
    expect(() => Argument.mustBeAnObject([], 'method', 'argument')).toThrow();
    expect(() => Argument.mustBeAnObject([0, 1, 2], 'method', 'argument')).toThrow();
    expect(() => Argument.mustBeAnObject(() => {}, 'method', 'argument')).toThrow();
    expect(() => Argument.mustBeAnObject(false, 'method', 'argument')).toThrow();
    expect(() => Argument.mustBeAnObject('', 'method', 'argument')).toThrow();
    expect(() => Argument.mustBeAnObject(0, 'method', 'argument')).toThrow();
    expect(() => Argument.mustBeAnObject(NaN, 'method', 'argument')).toThrow();
  });

  it("doesn't throw error when mustBeAnObject gets object value.", () => {
    expect(() => Argument.mustBeAnObject({}, 'method', 'argument')).not.toThrow();
  });

  it('throws error when mustBeAnArray gets non array value.', () => {
    expect(() => Argument.mustBeAnArray(null, 'method', 'argument')).toThrow();
    expect(() => Argument.mustBeAnArray(true, 'method', 'argument')).toThrow();
    expect(() => Argument.mustBeAnArray('test', 'method', 'argument')).toThrow();
    expect(() => Argument.mustBeAnArray(1, 'method', 'argument')).toThrow();
    expect(() => Argument.mustBeAnArray({}, 'method', 'argument')).toThrow();
    expect(() => Argument.mustBeAnArray(() => {}, 'method', 'argument')).toThrow();
    expect(() => Argument.mustBeAnArray(false, 'method', 'argument')).toThrow();
    expect(() => Argument.mustBeAnArray('', 'method', 'argument')).toThrow();
    expect(() => Argument.mustBeAnArray(0, 'method', 'argument')).toThrow();
    expect(() => Argument.mustBeAnArray(NaN, 'method', 'argument')).toThrow();
  });

  it("doesn't throw error when mustBeAnObject gets array value.", () => {
    expect(() => Argument.mustBeAnArray([], 'method', 'argument')).not.toThrow();
    expect(() => Argument.mustBeAnArray([0, 1, 2], 'method', 'argument')).not.toThrow();
  });

  it('throws error when mustBeNonEmptyString gets empty string value.', () => {
    expect(() => Argument.mustBeNonEmptyString('', 'method', 'argument')).toThrow();
  });

  it('throws error when mustBeNonEmptyString gets null value.', () => {
    expect(() => Argument.mustBeNonEmptyString(null, 'method', 'argument')).toThrow();
  });

  it('throws error when mustBeNonEmptyString gets undefined value.', () => {
    expect(() => Argument.mustBeNonEmptyString(undefined, 'method', 'argument')).toThrow();
  });

  it("doesn't throw error when mustBeAnObject gets non empty string value.", () => {
    expect(() => Argument.mustBeNonEmptyString('non empty string', 'method', 'argument')).not.toThrow();
  });

  it('throws error when mustBeType gets non function or class value.', () => {
    expect(() => Argument.mustBeType(null, 'method', 'argument')).toThrow();
    expect(() => Argument.mustBeType(true, 'method', 'argument')).toThrow();
    expect(() => Argument.mustBeType('test', 'method', 'argument')).toThrow();
    expect(() => Argument.mustBeType(1, 'method', 'argument')).toThrow();
    expect(() => Argument.mustBeType({}, 'method', 'argument')).toThrow();
    expect(() => Argument.mustBeType([], 'method', 'argument')).toThrow();
    expect(() => Argument.mustBeType([0, 1, 2], 'method', 'argument')).toThrow();
    expect(() => Argument.mustBeType(false, 'method', 'argument')).toThrow();
    expect(() => Argument.mustBeType('', 'method', 'argument')).toThrow();
    expect(() => Argument.mustBeType(0, 'method', 'argument')).toThrow();
    expect(() => Argument.mustBeType(NaN, 'method', 'argument')).toThrow();
  });

  it("doesn't throw error when mustBeType gets function value.", () => {
    expect(() => Argument.mustBeType(() => {}, 'method', 'argument')).not.toThrow();
  });

  it("doesn't throw error when mustBeType gets class value.", () => {
    class MockClass {}

    expect(() => Argument.mustBeType(MockClass, 'method', 'argument')).not.toThrow();
  });

  it('throws error when mustBeFunction gets non function or class value.', () => {
    expect(() => Argument.mustBeFunction(null, 'method', 'argument')).toThrow();
    expect(() => Argument.mustBeFunction(true, 'method', 'argument')).toThrow();
    expect(() => Argument.mustBeFunction('test', 'method', 'argument')).toThrow();
    expect(() => Argument.mustBeFunction(1, 'method', 'argument')).toThrow();
    expect(() => Argument.mustBeFunction({}, 'method', 'argument')).toThrow();
    expect(() => Argument.mustBeFunction([], 'method', 'argument')).toThrow();
    expect(() => Argument.mustBeFunction([0, 1, 2], 'method', 'argument')).toThrow();
    expect(() => Argument.mustBeFunction(false, 'method', 'argument')).toThrow();
    expect(() => Argument.mustBeFunction('', 'method', 'argument')).toThrow();
    expect(() => Argument.mustBeFunction(0, 'method', 'argument')).toThrow();
    expect(() => Argument.mustBeFunction(NaN, 'method', 'argument')).toThrow();
  });

  it("doesn't throw error when mustBeFunction gets function value.", () => {
    expect(() => Argument.mustBeFunction(() => {}, 'method', 'argument')).not.toThrow();
  });

  it("doesn't throw error when mustBeFunction gets class value.", () => {
    class MockClass {}

    expect(() => Argument.mustBeFunction(MockClass, 'method', 'argument')).not.toThrow();
  });

  it("throws error when mustEqual doesn't get equal values.", () => {
    expect(() => Argument.mustEqual(0, '0', 'method', 'argument')).toThrow();
    expect(() => Argument.mustEqual(0, false, 'method', 'argument')).toThrow();
    expect(() => Argument.mustEqual(0, null, 'method', 'argument')).toThrow();
    expect(() => Argument.mustEqual(0, undefined, 'method', 'argument')).toThrow();
    expect(() => Argument.mustEqual(0, '', 'method', 'argument')).toThrow();
    expect(() => Argument.mustEqual({}, {}, 'method', 'argument')).toThrow();
    expect(() => Argument.mustEqual([], [], 'method', 'argument')).toThrow();
    expect(() => Argument.mustEqual(() => {}, () => {}, 'method', 'argument')).toThrow();
  });

  it("doesn't throw error when mustEqual gets equal values.", () => {
    const mockObject: any = {};
    const mockArray: number[] = [];

    expect(() => Argument.mustEqual(true, true, 'method', 'argument')).not.toThrow();
    expect(() => Argument.mustEqual(0, 0, 'method', 'argument')).not.toThrow();
    expect(() => Argument.mustEqual('test', 'test', 'method', 'argument')).not.toThrow();
    expect(() => Argument.mustEqual(undefined, undefined, 'method', 'argument')).not.toThrow();
    expect(() => Argument.mustEqual(null, null, 'method', 'argument')).not.toThrow();
    expect(() => Argument.mustEqual(mockObject, mockObject, 'method', 'argument')).not.toThrow();
    expect(() => Argument.mustEqual(mockArray, mockArray, 'method', 'argument')).not.toThrow();
  });
});
