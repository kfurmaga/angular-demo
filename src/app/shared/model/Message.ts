export class Message {
  public static validationError(method: string, argumentName: string, reason: string): string {
    return `${method} argument ${argumentName} ${reason}.`;
  }

  public static canNotBeFalsy(method: string, argumentName: string): string {
    return Message.validationError(method, argumentName, "can't be falsy");
  }

  public static canNotBeNull(method: string, argumentName: string): string {
    return Message.validationError(method, argumentName, "can't be null");
  }

  public static canNotBeUndefined(method: string, argumentName: string): string {
    return Message.validationError(method, argumentName, "can't be undefined");
  }

  public static mustBeType(method: string, argumentName: string): string {
    return Message.validationError(method, argumentName, 'must be a type');
  }

  public static mustBeFunction(method: string, argumentName: string): string {
    return Message.validationError(method, argumentName, 'must be a function');
  }

  public static mustBeNotEmptyString(method: string, argumentName: string): string {
    return Message.validationError(method, argumentName, 'must be non empty string');
  }

  public static mustBeAnObject(method: string, argumentName: string): string {
    return Message.validationError(method, argumentName, 'must be an object');
  }

  public static mustBeAnArray(method: string, argumentName: string): string {
    return Message.validationError(method, argumentName, 'must be an array');
  }

  public static mustBe(method: string, argumentName: string, description: string): string {
    return Message.validationError(method, argumentName, `must be ${description}`);
  }

  public static mustEqual<TValue>(method: string, argumentName: string, value: TValue): string {
    return Message.validationError(method, argumentName, `must equal ${value}`);
  }
}
