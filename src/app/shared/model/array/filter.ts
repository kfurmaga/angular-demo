import {Argument} from '@app/shared/model/Argument';

export function distinct<TItem>(value: TItem, index: number, self: TItem[]): boolean {
  Argument.mustBeAnArray(self, 'Array.filter.distinct', 'self');

  if (index < 0 || index > self.length) {
    Argument.throwValidationError('Array.filter.distinct', 'index', 'must be in self range');
  }

  return self.indexOf(value) === index;
}

export function isInstanceOf(type: any): (o: any) => boolean {
  Argument.mustBeDefined(type, 'Array.filter.isInstanceOf', 'type');
  Argument.mustBeType(type, 'Array.filter.isInstanceOf', 'type');

  return (o: any): boolean => o instanceof type;
}

export function isNotNull(value: any): boolean {
  return value !== null;
}
