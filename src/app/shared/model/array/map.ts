import {Argument} from '@app/shared/model/Argument';

export function every<TItem, TResult>(itemToResult: (item: TItem) => TResult): (item: TItem[]) => TResult[] {
  Argument.mustBeFunction(itemToResult, 'Array.map.every', 'itemToResult');

  return (items: TItem[]): TResult[] => items.map(itemToResult);
}

export function reduceUsing<TResult>(
  reducer: (previous: TResult, current: TResult, index?: number, array?: TResult[]) => TResult
): (items: TResult[]) => TResult {
  Argument.mustBeFunction(reducer, 'Array.map.reduceWith', 'reducer');

  return (items: TResult[]): TResult => items.reduce(reducer);
}

export function toVoid(): void {
  return null;
}

export function everyItem<TItem>(
  predicate: (item: TItem) => boolean,
  defaultValue: boolean = false
): (items: TItem[]) => boolean {
  Argument.mustBeFunction(predicate, 'Array.map.everyItem', 'predicate');

  return (items: TItem[]): boolean => {
    if (items.length === 0) {
      return defaultValue;
    }

    return items.every(predicate);
  };
}

export function everyItemHaveTheSame<TItem, TValue>(getValue: (item: TItem) => TValue): (items: TItem[]) => boolean {
  Argument.mustBeFunction(getValue, 'Array.map.everyItemHaveTheSame', 'getValue');

  return (items: TItem[]): boolean => {
    if (items.length === 0) {
      return false;
    }

    const value: TValue = getValue(items[0]);
    return items.every((row) => getValue(row) === value);
  };
}

export function negate(value: boolean): boolean {
  return !value;
}
