import {and, or} from '@app/shared/model/array/reduce';

describe('Array.reduce.and', () => {
  it('returns true if both parameters are true', () => {
    expect(and(true, true)).toBe(true);
  });

  it('returns false if at least one of parameters is false', () => {
    expect(and(false, true)).toBe(false);
    expect(and(true, false)).toBe(false);
    expect(and(false, false)).toBe(false);
  });
});

describe('Array.reduce.or', () => {
  it('returns true if at least one of parameters is true', () => {
    expect(or(true, true)).toBe(true);
    expect(or(false, true)).toBe(true);
    expect(or(true, false)).toBe(true);
  });

  it('returns false if both parameters are false', () => {
    expect(or(false, false)).toBe(false);
  });
});
