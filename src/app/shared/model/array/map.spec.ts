import {every, everyItem, everyItemHaveTheSame, negate, reduceUsing, toVoid} from '@app/shared/model/array/map';
import Spy = jasmine.Spy;

describe('Array.map.every', () => {
  it('throws error if null is passed as parameter.', () => {
    expect(() => {
      every(null);
    }).toThrow();
  });

  it('throws error if null is passed as parameter.', () => {
    expect(() => {
      every(undefined);
    }).toThrow();
  });

  it('executes map function for every item', () => {
    const sut: (item: string[]) => string[] = every((item: string): string => item.toUpperCase());
    const list: string[] = ['a', 'b', 'c', 'd', 'e'];
    const expectedResult: string[] = ['A', 'B', 'C', 'D', 'E'];
    const result: string[] = sut(list);

    expect(result).toEqual(expectedResult);
  });
});

describe('Array.map.toVoid', () => {
  it('always return null', () => {
    expect(toVoid()).toBeNull();
  });
});

describe('Array.map.everyItem', () => {
  function isOdd(value: number): boolean {
    return value % 2 === 1;
  }

  it('throws error when null is passed as predicate', () => {
    expect(() => {
      everyItem(null);
    }).toThrow();
  });

  it('throws error when undefined is passed as predicate', () => {
    expect(() => {
      everyItem(undefined);
    }).toThrow();
  });

  it('returns defaultValue for empty array', () => {
    const sut: (items: number[]) => boolean = everyItem(isOdd, true);

    const result: boolean = sut([]);

    expect(result).toBe(true);
  });

  it('returns false for empty array when there is no defaultValue passed', () => {
    const sut: (items: number[]) => boolean = everyItem(isOdd);

    const result: boolean = sut([]);

    expect(result).toBe(false);
  });

  it('returns true when predicate returns true for all values', () => {
    const sut: (items: number[]) => boolean = everyItem(isOdd);

    const result: boolean = sut([1, 3, 5, 7]);

    expect(result).toBe(true);
  });

  it("returns false if predicate doesn't return true for all values", () => {
    const sut: (items: number[]) => boolean = everyItem(isOdd);

    const result: boolean = sut([1, 3, 4, 7]);

    expect(result).toBe(false);
  });
});

describe('Array.map.everyItemHaveTheSame', () => {
  class MockClass {
    public constructor(public readonly value: string) {}
  }

  it('throws error when null is passed as predicate', () => {
    expect(() => {
      everyItemHaveTheSame(null);
    }).toThrow();
  });

  it('throws error when undefined is passed as predicate', () => {
    expect(() => {
      everyItemHaveTheSame(undefined);
    }).toThrow();
  });

  it('returns true when getValue returns always the same value', () => {
    const sut: (items: MockClass[]) => boolean = everyItemHaveTheSame((item) => item.value);

    const result: boolean = sut([
      new MockClass('a'),
      new MockClass('a'),
      new MockClass('a'),
      new MockClass('a'),
      new MockClass('a'),
    ]);

    expect(result).toBe(true);
  });

  it("returns false if predicate doesn't return true for all values", () => {
    const sut: (items: MockClass[]) => boolean = everyItemHaveTheSame((item) => item.value);

    const result: boolean = sut([
      new MockClass('a'),
      new MockClass('a'),
      new MockClass('b'),
      new MockClass('a'),
      new MockClass('a'),
    ]);

    expect(result).toBe(false);
  });

  it("returns false if empty array is passed", () => {
    const sut: (items: MockClass[]) => boolean = everyItemHaveTheSame((item) => item.value);

    const result: boolean = sut([]);

    expect(result).toBe(false);
  });
});

describe('Array.map.negate', () => {
  it('returns true when false was passed as argument', () => {
    expect(negate(false)).toBe(true);
  });

  it('returns false when true was passed as argument', () => {
    expect(negate(true)).toBe(false);
  });
});

describe('Array.map.reduceUsing', () => {
  it('throws error if null is passed as parameter.', () => {
    expect(() => {
      reduceUsing(null);
    }).toThrow();
  });

  it('throws error if null is passed as parameter.', () => {
    expect(() => {
      reduceUsing(undefined);
    }).toThrow();
  });

  it('reduces value with passed value', () => {
    const sut: (item: number[]) => number = reduceUsing(
      (previous: number, current: number): number => previous + current
    );
    const list: number[] = [1, 2, 3, 4, 5];
    const expectedResult: number = 15;
    const result: number = sut(list);

    expect(result).toBe(expectedResult);
  });

  it('passes current of combined element and whole array to reducer', () => {
    const observer: {callback: (p: string, c: string) => string} = {
      callback: (p: string, c: string): string => c,
    };
    const callback: Spy = spyOn(observer, 'callback').and.callThrough();

    const sut: (item: string[]) => string = reduceUsing(callback);
    const list: string[] = ['a', 'b', 'c', 'd', 'e'];
    const result: string = sut(list);

    expect(callback).toHaveBeenCalledTimes(list.length - 1);
    expect(callback).toHaveBeenCalledWith('a', 'b', 1, list);
    expect(callback).toHaveBeenCalledWith('b', 'c', 2, list);
    expect(callback).toHaveBeenCalledWith('c', 'd', 3, list);
    expect(callback).toHaveBeenCalledWith('d', 'e', 4, list);
  });
});
