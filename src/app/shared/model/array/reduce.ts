export function and(previous: boolean, current: boolean): boolean {
  return previous && current;
}

export function or(previous: boolean, current: boolean): boolean {
  return previous || current;
}
