import {distinct, isInstanceOf, isNotNull} from '@app/shared/model/array/filter';

describe('Array.filter.distinct', () => {
  const list: string[] = ['a', 'a', 'b', 'c', 'd', 'd', 'd', 'e', 'e', 'e', 'e', 'e'];

  it('throws error when null is passed as self', () => {
    expect(() => {
      distinct('a', 0, null);
    }).toThrow();
  });

  it('throws error when undefined is passed as self', () => {
    expect(() => {
      distinct('a', 0, undefined);
    }).toThrow();
  });

  it('throws error when invalid index is passed', () => {
    expect(() => {
      distinct('a', -1, list);
    }).toThrow();

    expect(() => {
      distinct('a', list.length + 1, list);
    }).toThrow();
  });

  it("returns true if passed value is distinct or it's a first occur.", () => {
    expect(distinct('a', 0, list)).toEqual(true);
    expect(distinct('b', 2, list)).toEqual(true);
    expect(distinct('c', 3, list)).toEqual(true);
    expect(distinct('d', 4, list)).toEqual(true);
    expect(distinct('e', 7, list)).toEqual(true);
  });

  it("returns false if passed value is not distinct and it's not a first occur.", () => {
    expect(distinct('a', 1, list)).toEqual(false);
    expect(distinct('d', 5, list)).toEqual(false);
    expect(distinct('d', 6, list)).toEqual(false);
    expect(distinct('e', 8, list)).toEqual(false);
    expect(distinct('e', 9, list)).toEqual(false);
    expect(distinct('e', 10, list)).toEqual(false);
    expect(distinct('e', 11, list)).toEqual(false);
  });
});

describe('Array.filter.isInstanceOf', () => {
  class TestClass {}

  class OtherClass {}

  function functionClass(): void {}

  it('throws error when null is passed.', () => {
    expect(() => {
      isInstanceOf(null);
    }).toThrow();
  });

  it('throws error when null is passed.', () => {
    expect(() => {
      isInstanceOf(undefined);
    }).toThrow();
  });

  it('throws error if string is passed as type.', () => {
    expect(() => {
      isInstanceOf('string');
    }).toThrow();
  });

  it('throws error if number is passed as type.', () => {
    expect(() => {
      isInstanceOf(0);
    }).toThrow();
  });

  it('throws error if boolean is passed as type.', () => {
    expect(() => {
      isInstanceOf(true);
    }).toThrow();
  });

  it('throws error if object is passed as type.', () => {
    expect(() => {
      isInstanceOf({});
    }).toThrow();
  });

  it('creates isInstanceOf filter for passed class.', () => {
    const sut: Function = isInstanceOf(TestClass);

    expect(sut(new functionClass())).toEqual(false);
    expect(sut(new TestClass())).toEqual(true);
    expect(sut(new OtherClass())).toEqual(false);
    expect(sut(null)).toEqual(false);
    expect(sut(undefined)).toEqual(false);
    expect(sut(0)).toEqual(false);
    expect(sut('')).toEqual(false);
    expect(sut('test')).toEqual(false);
    expect(sut(false)).toEqual(false);
    expect(sut(true)).toEqual(false);
  });

  it('creates isInstanceOf filter for function.', () => {
    const sut: Function = isInstanceOf(functionClass);

    expect(sut(new functionClass())).toEqual(true);
    expect(sut(new TestClass())).toEqual(false);
    expect(sut(new OtherClass())).toEqual(false);
    expect(sut(null)).toEqual(false);
    expect(sut(undefined)).toEqual(false);
    expect(sut(0)).toEqual(false);
    expect(sut('')).toEqual(false);
    expect(sut('test')).toEqual(false);
    expect(sut(false)).toEqual(false);
    expect(sut(true)).toEqual(false);
  });
});

describe('Array.filter.isNotNull', () => {
  it('returns true if passed parameter is not null.', () => {
    expect(isNotNull('')).toEqual(true);
    expect(isNotNull('test')).toEqual(true);
    expect(isNotNull(0)).toEqual(true);
    expect(isNotNull(1)).toEqual(true);
    expect(isNotNull(undefined)).toEqual(true);
    expect(isNotNull(true)).toEqual(true);
    expect(isNotNull(false)).toEqual(true);
    expect(isNotNull({})).toEqual(true);
    expect(isNotNull(() => {})).toEqual(true);
  });

  it('returns false if passed parameter is null', () => {
    expect(isNotNull(null)).toEqual(false);
  });
});
