import {Message} from '@app/shared/model/Message';

export class Argument {
  public static throwValidationError(method: string, argumentName: string, reason: string): void {
    throw new Error(Message.validationError(method, argumentName, reason));
  }

  public static canNotBeFalsy<TValue>(value: TValue, method: string, argumentName: string): void {
    if (!value) {
      throw new Error(Message.canNotBeFalsy(method, argumentName));
    }
  }

  public static canNotBeNull<TValue>(value: TValue, method: string, argumentName: string): void {
    if (value === null) {
      throw new Error(Message.canNotBeNull(method, argumentName));
    }
  }

  public static canNotBeUndefined<TValue>(value: TValue, method: string, argumentName: string): void {
    if (value === undefined) {
      throw new Error(Message.canNotBeUndefined(method, argumentName));

    }
  }

  public static mustBeDefined<TValue>(value: TValue, method: string, argumentName: string): void {
    this.canNotBeNull(value, method, argumentName);
    this.canNotBeUndefined(value, method, argumentName);
  }

  public static mustBeType(value: any, method: string, argumentName: string): void {
    if (typeof value !== 'function') {
      throw new Error(Message.mustBeType(method, argumentName));
    }
  }

  public static mustBeFunction(value: any, method: string, argumentName: string): void {
    if (typeof value !== 'function') {
      throw new Error(Message.mustBeFunction(method, argumentName));
    }
  }

  public static mustBeNonEmptyString(value: string, method: string, argumentName: string): void {
    if (!value) {
      throw new Error(Message.mustBeNotEmptyString(method, argumentName));
    }
  }

  public static mustBeAnObject(value: any, method: string, argumentName: string): void {
    if (!value || Array.isArray(value) || typeof value !== 'object') {
      throw new Error(Message.mustBeAnObject(method, argumentName));
    }
  }

  public static mustBeAnArray(value: any, method: string, argumentName: string): void {
    if (!Array.isArray(value)) {
      throw new Error(Message.mustBeAnArray(method, argumentName));
    }
  }

  public static mustBe(description: string, assertionResult: boolean, method: string, argumentName: string): void {
    if (!assertionResult) {
      throw new Error(Message.mustBe(method, argumentName, description));
    }
  }

  public static mustEqual(value: any, expectedValue: any, method: string, argumentName: string): void {
    if (value !== expectedValue) {
      throw new Error(Message.mustEqual(method, argumentName, value));
    }
  }
}
