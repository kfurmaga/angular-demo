import {ActivatedRoute, ActivatedRouteSnapshot, Data, Params, UrlSegment} from '@angular/router';
import {Argument} from '@app/shared/model/Argument';
import {BehaviorSubject} from 'rxjs';

export class MockActivatedRoute extends ActivatedRoute {
  private readonly _data: BehaviorSubject<Data>;
  private readonly _params: BehaviorSubject<Params>;
  private readonly _queryParams: BehaviorSubject<Params>;

  public constructor(data: Data = {}, params: Params = {}, queryParams: Params = {}) {
    super();

    const url: UrlSegment[] = [new UrlSegment('mock', {})];

    this.snapshot = new ActivatedRouteSnapshot();
    this.snapshot.url = url;
    this.snapshot.data = data;
    this.snapshot.params = params;
    this.snapshot.queryParams = queryParams;

    this._data = new BehaviorSubject(data);
    this._params = new BehaviorSubject(params);
    this._queryParams = new BehaviorSubject(queryParams);

    this.url = new BehaviorSubject(url);
    this.data = this._data;
    this.params = this._params;
    this.queryParams = this._queryParams;
  }

  public updateData<TValue>(key: string, value: TValue): void {
    Argument.mustBe(`passed in initial data in '${key}' field`, this.snapshot.data[key] !== undefined, 'MockActivatedRoute.updateData', 'key');

    const updatedData: Data = {
      ...this.snapshot.data,
      [key]: value
    };

    this.snapshot.data = updatedData;
    this._data.next(updatedData);
  }

  public updateParam<TValue>(key: string, value: TValue): void {
    Argument.mustBe(`passed in initial params in '${key}' field`, this.snapshot.params[key] !== undefined, 'MockActivatedRoute.updateParam', 'key');

    const updatedParams: Params = {
      ...this.snapshot.params,
      [key]: value
    };

    this.snapshot.params = updatedParams;
    this._params.next(updatedParams);
  }

  public updateQueryParam<TValue>(key: string, value: TValue): void {
    Argument.mustBe(`passed in initial queryParams in '${key}' field`, this.snapshot.queryParams[key] !== undefined, 'MockActivatedRoute.updateQueryParam', 'key');

    const updatedQueryParams: Params = {
      ...this.snapshot.queryParams,
      [key]: value
    };

    this.snapshot.queryParams = updatedQueryParams;
    this._queryParams.next(updatedQueryParams);
  }
}
