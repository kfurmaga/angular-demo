import {Data, Params} from '@angular/router';
import {Message} from '@app/shared/model/Message';
import {MockActivatedRoute} from '@app/shared/testing/MockActivatedRoute';

describe('MockActivatedRoute', () => {
  const initialData: Data = {
    test: 'value',
  };
  const initialParams: Params = {
    a: 'A',
  };
  const initialQueryParams: Params = {
    b: 'B',
  };

  describe('constructor', () => {

    it('creates instance of ActivatedRoute', () => {
      const sut: MockActivatedRoute = new MockActivatedRoute();
      expect(sut).not.toBeNull();
    });

    it('sets passed data in snapshot.data', () => {
      const sut: MockActivatedRoute = new MockActivatedRoute(initialData);
      expect(sut.snapshot.data).toBe(initialData);
    });

    it('sets passed data as data observable initial value', (done: DoneFn) => {
      const sut: MockActivatedRoute = new MockActivatedRoute(initialData);

      sut.data.subscribe(
        (data) => {
          expect(data).toBe(initialData);
          done();
        },
        () => done.fail()
      );
    });

    it('sets passed params in snapshot.params', () => {
      const sut: MockActivatedRoute = new MockActivatedRoute(undefined, initialParams);
      expect(sut.snapshot.params).toBe(initialParams);
    });

    it('sets passed params as params observable initial value', (done: DoneFn) => {
      const sut: MockActivatedRoute = new MockActivatedRoute(undefined, initialParams);

      sut.params.subscribe(
        (params) => {
          expect(params).toBe(initialParams);
          done();
        },
        () => done.fail()
      );
    });

    it('sets passed queryParams in snapshot.queryParams', () => {
      const sut: MockActivatedRoute = new MockActivatedRoute(undefined, undefined, initialQueryParams);
      expect(sut.snapshot.queryParams).toBe(initialQueryParams);
    });

    it('sets passed queryParams as queryParams observable initial value', (done: DoneFn) => {
      const sut: MockActivatedRoute = new MockActivatedRoute(undefined, undefined, initialQueryParams);

      sut.queryParams.subscribe(
        (queryParams) => {
          expect(queryParams).toBe(initialQueryParams);
          done();
        },
        () => done.fail()
      );
    });
  });

  describe('updateData', () => {
    const expectedData: Data = {
      ...initialData,
      test: 'updated test'
    };
    let sut: MockActivatedRoute;

    beforeEach(() => {
      sut = new MockActivatedRoute(initialData);
      sut.updateData('test', expectedData.test);
    });

    it ('throws error when key that didn\'t exist in initial data was passed', () => {
      expect(() => {
        sut.updateData('nonExistingKey', 'some value');
      }).toThrowError(Message.mustBe('MockActivatedRoute.updateData', 'key', 'passed in initial data in \'nonExistingKey\' field'));
    });

    it('updates passed data in snapshot.data', () => {
      expect(sut.snapshot.data).toEqual(expectedData);
    });

    it('updates data in data observable', (done: DoneFn) => {
      sut.data.subscribe(
        (data) => {
          expect(data).toEqual(expectedData);
          done();
        },
        () => done.fail()
      );
    });
  });

  describe('updateParam', () => {
    const expectedParams: Params = {
      ...initialParams,
      a: 'updated A'
    };
    let sut: MockActivatedRoute;

    beforeEach(() => {
      sut = new MockActivatedRoute(undefined, initialParams);
      sut.updateParam('a', expectedParams.a);
    });

    it ('throws error when key that didn\'t exist in initial data was passed', () => {
      expect(() => {
        sut.updateParam('nonExistingKey', 'some value');
      }).toThrowError(Message.mustBe( 'MockActivatedRoute.updateParam', 'key', 'passed in initial params in \'nonExistingKey\' field'));
    });

    it('updates passed param in snapshot.params', () => {
      expect(sut.snapshot.params).toEqual(expectedParams);
    });

    it('updates value in params observable', (done: DoneFn) => {
      sut.params.subscribe(
        (params) => {
          expect(params).toEqual(expectedParams);
          done();
        },
        () => done.fail()
      );
    });
  });

  describe('updateQueryParam', () => {
    const expectedQueryParams: Params = {
      ...initialQueryParams,
      b: 'updated B'
    };
    let sut: MockActivatedRoute;

    beforeEach(() => {
      sut = new MockActivatedRoute(undefined, undefined, initialQueryParams);
      sut.updateQueryParam('b', expectedQueryParams.b);
    });

    it ('throws error when key that didn\'t exist in initial data was passed', () => {
      expect(() => {
        sut.updateQueryParam('nonExistingKey', 'some value');
      }).toThrowError(Message.mustBe('MockActivatedRoute.updateQueryParam', 'key', 'passed in initial queryParams in \'nonExistingKey\' field'));
    });

    it('updates passed param in snapshot.params', () => {
      expect(sut.snapshot.queryParams).toEqual(expectedQueryParams);
    });

    it('updates value in params observable', (done: DoneFn) => {
      sut.queryParams.subscribe(
        (queryParams) => {
          expect(queryParams).toEqual(expectedQueryParams);
          done();
        },
        () => done.fail()
      );
    });
  });
});
