import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {
    path: 'examples',
    loadChildren: () => import('./examples/ExamplesModule').then(m => m.ExamplesModule),
  },
  {
    path: '',
    redirectTo: '/examples',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {paramsInheritanceStrategy: 'always'})],
  exports: [RouterModule],
})
export class AppRoutingModule {}
