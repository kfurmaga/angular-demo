import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppComponent} from '@app/AppComponent';
import {AppRoutingModule} from '@app/AppRoutingModule';
import {createProviders as createExampleModuleProviders} from '@app/examples/injectables';
import {ProtocolsModule} from '@app/protocols/ProtocolsModule';

const interval: number = 1000;

export function getNow(): Date {
  return new Date();
}

@NgModule({
  declarations: [AppComponent],
  imports: [
    HttpClientModule,
    ProtocolsModule.forRoot({
      assets: '/assets/'
    }),
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
  ],
  providers: [
    ...createExampleModuleProviders(interval, getNow, setInterval)
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
