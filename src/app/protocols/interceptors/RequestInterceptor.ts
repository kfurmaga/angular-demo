import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {ProtocolService} from "@app/protocols/services/ProtocolService";
import {Observable} from 'rxjs';

@Injectable()
export class RequestInterceptor implements HttpInterceptor {
  constructor(private readonly protocolService: ProtocolService) {}
  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const newRequest: HttpRequest<any> = req.clone({
      url: this.protocolService.replaceProtocols(req.url),
    });
    return next.handle(newRequest);
  }
}
