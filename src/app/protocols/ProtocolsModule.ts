import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {ModuleWithProviders, NgModule} from '@angular/core';
import {Protocols, PROTOCOLS} from '@app/protocols/ProtocolsInjectables';
import {RequestInterceptor} from '@app/protocols/interceptors/RequestInterceptor';
import {ProtocolService} from '@app/protocols/services/ProtocolService';

const components: any[] = [];

const resolvers: any[] = [];

const mappings: any[] = [];

const pages: any[] = [];

const guards: any[] = [];

const clients: any[] = [];

const services: any[] = [ProtocolService];

const interceptors: any[] = [
  {
    provide: HTTP_INTERCEPTORS,
    useClass: RequestInterceptor,
    multi: true,
  },
];

@NgModule({
  imports: [],
  exports: [...components, ...pages],
  declarations: [...components, ...pages],
  providers: [...clients, ...mappings, ...resolvers, ...guards],
})
export class ProtocolsModule {
  public static forRoot(protocols: Protocols): ModuleWithProviders {
    return {
      ngModule: ProtocolsModule,
      providers: [
        ...interceptors,
        ...services,
        {
          provide: PROTOCOLS,
          useValue: protocols,
        },
      ],
    };
  }
}
