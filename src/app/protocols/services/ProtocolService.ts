import {Inject, Injectable} from '@angular/core';
import {Protocols, PROTOCOLS} from '@app/protocols/ProtocolsInjectables';
import {Argument} from '@app/shared/model/Argument';

@Injectable()
export class ProtocolService {
  private static readonly PROTOCOL_SEPEARATOR: string = '://';
  constructor(@Inject(PROTOCOLS) private readonly _protocols: Protocols) {
    Argument.mustBeDefined(_protocols, 'ProtocolService.constructor', '_protocols');
  }

  public replaceProtocols(url: string): string {
    Argument.canNotBeNull(url, 'ProtocolService.replaceProtocols', 'url');
    Argument.mustBeDefined(url, 'ProtocolService.replaceProtocols', 'url');

    const protocolName: string = Object.keys(this._protocols).find(
      (key) => url.indexOf(this.getProtocol(key)) === 0
    );
    if (!protocolName) {
      return url;
    }
    const protocol: string = this.getProtocol(protocolName);
    const protocolUrl: string = this._protocols[protocolName];
    return url.replace(protocol, protocolUrl);
  }

  private getProtocol(name: string): string {
    return name + ProtocolService.PROTOCOL_SEPEARATOR;
  }
}
