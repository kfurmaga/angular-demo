import {ProtocolService} from '@app/protocols/services/ProtocolService';
import {Argument} from '@app/shared/model/Argument';
import {Message} from '@app/shared/model/Message';

describe('ProtocolService', () => {
  describe('constructor', () => {
    it('creates ProtocolService', () => {
      const sut: ProtocolService = new ProtocolService({});
      expect(sut).not.toBeNull();
    });

    it('throws an error when null is passed as protocols', () => {
      expect(() => {
        const sut: ProtocolService = new ProtocolService(null);
      }).toThrow();
    });
  });
  describe('replaceProtocol', () => {
    it("returns the same url when doesn't contain defined protocol", () => {
      const sut: ProtocolService = new ProtocolService({test: 'eee'});
      const url: string = 'http://skaldy.com';
      const result: string = sut.replaceProtocols(url);
      expect(result).toBe(url);
    });

    it('returns changed url when it contains defined protocol', () => {
      const sut: ProtocolService = new ProtocolService({test: '/temp/'});
      const url: string = 'test://skaldy.com';
      const expectedUrl: string = '/temp/skaldy.com';
      const result: string = sut.replaceProtocols(url);
      expect(result).toBe(expectedUrl);
    });
    it("doesn't change the url if there are no two slashes in the address", () => {
      const sut: ProtocolService = new ProtocolService({test: '/temp'});
      const url: string = 'test:/skaldy.com';
      const result: string = sut.replaceProtocols(url);
      expect(result).toBe(url);
    });

    it("doesn't change url when url is not starting with protocol", () => {
      const sut: ProtocolService = new ProtocolService({test: '/temp'});
      const url: string = 'http://skaldytest://.com';
      const expectedUrl: string = url;
      const result: string = sut.replaceProtocols(url);
      expect(result).toBe(expectedUrl);
    });

    it('throws an error when null is passed as url', () => {
      expect(() => {
        const sut: ProtocolService = new ProtocolService({test: '/temp'});
        const url: string = null;
        sut.replaceProtocols(url);
      }).toThrowError(Message.canNotBeNull('ProtocolService.replaceProtocols', 'url'));
    });

    it('throws an error when undefined is passed as url', () => {
      expect(() => {
        const sut: ProtocolService = new ProtocolService({test: '/temp'});
        const url: string = undefined;
        sut.replaceProtocols(url);
      }).toThrowError(Message.canNotBeUndefined('ProtocolService.replaceProtocols', 'url'));
    });
  });
});
