export const PROTOCOLS: string = 'ProtocolsModule.protocols';
export interface Protocols {
  [protocolName: string]: string;
}
