import {InjectionToken, Provider} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

export type GetNow = () => Date;
export type SetInterval = (handler: TimerHandler, timeout: number) => number;

export const INTERVAL: InjectionToken<number> = new InjectionToken('ExamplesModule.interval');
export const GET_NOW: InjectionToken<GetNow> = new InjectionToken('ExamplesModule.getNow');
export const SET_INTERVAL: InjectionToken<SetInterval> = new InjectionToken('ExamplesModule.setInterval');

export function createProviders(interval: number, getNow: GetNow, setInterval: SetInterval): Provider[] {
  return [
    {
      provide: INTERVAL,
      useValue: interval,
    },
    {
      provide: GET_NOW,
      useValue: getNow,
    },
    {
      provide: SET_INTERVAL,
      useValue: setInterval,
    },
  ];
}
