import {Params} from '@angular/router';

export function getItemId(params: Params): number {
  return +params.itemId;
}
