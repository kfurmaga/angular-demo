import {Data} from '@angular/router';
import {ListItem} from '@app/examples/model/ListItem';

export function getRoar(data: Data): string {
  return data.roar;
}

export function getListItem(data: Data): ListItem {
  return data.listItem;
}
