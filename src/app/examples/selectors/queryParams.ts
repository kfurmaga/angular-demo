import {Params} from '@angular/router';

export function getMagicWord(queryParams: Params): string {
  return queryParams.magicWord;
}
