import {TestBed} from '@angular/core/testing';
import {InjectablesMock, TestExamplesModule} from '@app/examples/TestExamplesModule';
import {AgoPipe} from '@app/examples/pipes/AgoPipe';
import {Message} from '@app/shared/model/Message';

describe('AgoPipe', () => {
  let sut: AgoPipe;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestExamplesModule.forRoot()],
    });
    sut = TestBed.get(AgoPipe);
  });

  it('creates instance of pipe', () => {
    expect(sut).not.toBeNull();
  });

  describe('transform', () => {
    it('throws an error when null is passed as parameter', () => {
      expect(() => {
        sut.transform(null);
      }).toThrowError(Message.canNotBeNull('AgoPipe.transform', 'value'));
    });

    it('returns positive value when passed value is after now', () => {
      const injectables: InjectablesMock = TestBed.get(InjectablesMock);
      injectables.setNow(new Date('29 Apr 1986 07:45:26 UTC'));
      sut.transform(new Date('29 Apr 1986 07:45:23 UTC')).subscribe((value) => expect(value).toBe(3));
    });

    it('returns negative value when passed value is after now', () => {
      const injectables: InjectablesMock = TestBed.get(InjectablesMock);
      injectables.setNow(new Date('29 Apr 1986 07:45:20 UTC'));
      sut.transform(new Date('29 Apr 1986 07:45:23 UTC')).subscribe((value) => expect(value).toBe(-3));
    });

    it('returns positive value when passed value is after now', () => {
      const injectables: InjectablesMock = TestBed.get(InjectablesMock);
      injectables.setNow(new Date('29 Apr 1986 07:45:23 UTC'));
      sut.transform(new Date('29 Apr 1986 07:45:23 UTC')).subscribe((value) => expect(value).toBe(0));
    });
  });
});
