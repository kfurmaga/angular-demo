import {Pipe, PipeTransform} from '@angular/core';
import {ParallelService} from '@app/examples/services/ParallelService';
import {Argument} from '@app/shared/model/Argument';
import {Observable} from 'rxjs';

@Pipe({
  name: 'ago',
})
export class AgoPipe implements PipeTransform {
  public constructor(private readonly _parallelService: ParallelService) {}

  public transform(value: Date): Observable<number> {
    Argument.canNotBeNull(value, 'AgoPipe.transform', 'value');

    return this._parallelService.now
      .map((now) => Math.round((now.getTime() - value.getTime()) / 1000));
  }
}
