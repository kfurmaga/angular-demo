import {Pipe, PipeTransform} from '@angular/core';
import {SimpleService} from '@app/examples/services/SimpleService';

@Pipe({
  name: 'injected'
})
export class InjectedServicePipe implements PipeTransform {
  public constructor(private readonly _simpleService: SimpleService) {}

  public transform(value: string): string {
    return `${this._simpleService.bar} ${value}`;
  }
}
