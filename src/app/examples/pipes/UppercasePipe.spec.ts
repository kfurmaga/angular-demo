import {TestBed} from '@angular/core/testing';
import {TestExamplesModule} from '@app/examples/TestExamplesModule';
import {UppercasePipe} from '@app/examples/pipes/UppercasePipe';

describe('UppercasePipe', () => {
  let sut: UppercasePipe;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestExamplesModule.forRoot()],
    });
    sut = TestBed.get(UppercasePipe);
  });

  it('creates instance of pipe', () => {
    expect(sut).not.toBeNull();
  });

  describe('transform', () => {
    it('throws an error when null is passed as parameter', () => {
      expect(() => {
        sut.transform(null);
      }).toThrow();
    });

    it('transforms passed string to uppercase ', () => {
      expect(sut.transform('abc')).toBe('ABC');
    });
  });
});
