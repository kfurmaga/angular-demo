import {TestBed} from '@angular/core/testing';
import {TestExamplesModule} from '@app/examples/TestExamplesModule';
import {InjectedServicePipe} from '@app/examples/pipes/InjectedServicePipe';

describe('InjectedServicePipe', () => {
  let sut: InjectedServicePipe;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestExamplesModule.forRoot()],
    });
    sut = TestBed.get(InjectedServicePipe);
  });

  it('creates instance of pipe', () => {
    expect(sut).not.toBeNull();
  });

  describe('transform', () => {
    it('doesn\'t throw error when null is passed as parameter', () => {
      expect(() => {
        sut.transform(null);
      }).not.toThrow();
    });

    it('returns result containing passed value', () => {
      expect(sut.transform('abc')).toContain('abc');
    });
  });
});
