export interface MovieDto {
  title: string;
  description: string;
}

export namespace MovieDto {
  export function createMock(id: number): MovieDto {
    return {
      title: `MovieDto.${id}.title`,
      description: `MovieDto.${id}.description`,
    };
  }
}
