import {MovieDto} from '@app/examples/dto/MovieDto';
import {ListItem} from '@app/examples/model/ListItem';
import {SynchronousMapping} from '@app/shared/mappings/SynchronousMapping';

export class MovieDtoToListItemMapping extends SynchronousMapping<MovieDto, ListItem> {
  public static readonly EXCERPT_LENGTH: number = 30;
  public static readonly ELLIPSIS_SUFFIX: string = '...';

  protected map(source: MovieDto): ListItem {
    return new ListItem(
      source.title,
      this.ellipsis(source.description),
    );
  }

  private ellipsis(value: string): string {
    return value.length > MovieDtoToListItemMapping.EXCERPT_LENGTH
      ? value.slice(0, MovieDtoToListItemMapping.EXCERPT_LENGTH) + MovieDtoToListItemMapping.ELLIPSIS_SUFFIX
      : value;
  }
}
