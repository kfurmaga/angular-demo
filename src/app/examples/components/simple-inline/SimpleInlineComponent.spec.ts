import {ComponentFixture, TestBed} from '@angular/core/testing';
import {TestExamplesModule} from '@app/examples/TestExamplesModule';
import {SimpleInlineComponent} from '@app/examples/components/simple-inline/SimpleInlineComponent';

describe('SimpleInlineComponent', () => {
  let fixture: ComponentFixture<SimpleInlineComponent>;
  let sut: SimpleInlineComponent;
  let dom: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestExamplesModule.forRoot()],
    });
    fixture = TestBed.createComponent(SimpleInlineComponent);
    sut = fixture.componentInstance;
    dom = fixture.nativeElement;
  });

  it('creates instance of component', () => {
    expect(sut).not.toBeNull();
  });

  it('renders component', () => {
    expect(dom).not.toBeNull();
  });
});
