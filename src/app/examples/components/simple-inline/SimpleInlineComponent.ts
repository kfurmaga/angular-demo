import {Component} from '@angular/core';

@Component({
  selector: 'app-simple-inline',
  template:
    "<p>This is a component with a template in defined in component file. It's possible but should <strong>NEVER</strong> use this or you should have a good excuse...</p>",
})
export class SimpleInlineComponent {}
