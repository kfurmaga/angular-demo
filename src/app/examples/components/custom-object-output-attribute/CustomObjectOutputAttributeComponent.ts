import {Component, EventEmitter, Output} from '@angular/core';
import {CustomObject} from '@app/examples/model/CustomObject';

@Component({
  selector: 'app-custom-object-output-attribute',
  templateUrl: 'CustomObjectOutputAttributeComponent.html',
})
export class CustomObjectOutputAttributeComponent {
  @Output()
  public fiz: EventEmitter<CustomObject> = new EventEmitter();

  public onClick(event: MouseEvent): void {
    const orderNumber: string = new Date().getSeconds().toString();
    const customObject: CustomObject = new CustomObject(orderNumber);
    this.fiz.emit(customObject);
  }
}
