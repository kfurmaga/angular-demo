import {ComponentFixture, TestBed} from '@angular/core/testing';
import {TestExamplesModule} from '@app/examples/TestExamplesModule';
import {CustomObjectOutputAttributeComponent} from '@app/examples/components/custom-object-output-attribute/CustomObjectOutputAttributeComponent';
import {CustomObject} from '@app/examples/model/CustomObject';
import createSpy = jasmine.createSpy;

describe('CustomObjectOutputAttributeComponent', () => {
  class MockType {}

  let fixture: ComponentFixture<CustomObjectOutputAttributeComponent>;
  let sut: CustomObjectOutputAttributeComponent;
  let dom: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestExamplesModule.forRoot()],
    });
    fixture = TestBed.createComponent(CustomObjectOutputAttributeComponent);
    sut = fixture.componentInstance;
    dom = fixture.nativeElement;
  });

  it('creates instance of component', () => {
    expect(sut).not.toBeNull();
  });

  it('renders component', () => {
    expect(dom).not.toBeNull();
  });

  describe('onClick', () => {
    it ('triggers fiz output', () => {
      const callback: (customObject: CustomObject) => void = createSpy();
      sut.fiz.subscribe(callback);
      sut.onClick(new MouseEvent('click'));
      expect(callback).toHaveBeenCalled();
    });
  });
});
