import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-number-input-attribute',
  templateUrl: 'NumberInputAttributeComponent.html'
})
export class NumberInputAttributeComponent {
  private _bar: number;
  @Input()
  public get bar(): number {
    return this._bar;
  }
  public set bar(value: number) {
    this._bar = value;
    this.barType = typeof value;
  }

  public barType: string = '';
}
