import {ComponentFixture, TestBed} from '@angular/core/testing';
import {TestExamplesModule} from '@app/examples/TestExamplesModule';
import {NumberInputAttributeComponent} from '@app/examples/components/number-input-attribute/NumberInputAttributeComponent';
import {CustomObject} from '@app/examples/model/CustomObject';

describe('NumberInputAttributeComponent', () => {
  class MockType {}

  let fixture: ComponentFixture<NumberInputAttributeComponent>;
  let sut: NumberInputAttributeComponent;
  let dom: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestExamplesModule.forRoot()],
    });
    fixture = TestBed.createComponent(NumberInputAttributeComponent);
    sut = fixture.componentInstance;
    dom = fixture.nativeElement;
  });

  it('creates instance of component', () => {
    expect(sut).not.toBeNull();
  });

  it('renders component', () => {
    expect(dom).not.toBeNull();
  });

  it('stores value set to bar', () => {
    const value: number = 333;
    expect(sut.bar).not.toBe(value);
    sut.bar = value;
    expect(sut.bar).toBe(value);
  });

  it('initially have barType set as empty string', () => {
    expect(sut.barType).toBe('');
  });

  it('sets barType to type of passed value', () => {
    const values: any[] = [true, false, 0, 333, null, undefined, '', 'test', new Date(), new MockType()];

    values.forEach((value) => {
      sut.bar = value;
      expect(sut.barType).toBe(typeof value);
    });
  });
});
