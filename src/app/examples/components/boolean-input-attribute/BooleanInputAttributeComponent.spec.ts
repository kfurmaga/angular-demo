import {ComponentFixture, TestBed} from '@angular/core/testing';
import {TestExamplesModule} from '@app/examples/TestExamplesModule';
import {BooleanInputAttributeComponent} from '@app/examples/components/boolean-input-attribute/BooleanInputAttributeComponent';

describe('BooleanInputAttributeComponent', () => {
  class MockType {}

  let fixture: ComponentFixture<BooleanInputAttributeComponent>;
  let sut: BooleanInputAttributeComponent;
  let dom: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestExamplesModule.forRoot()],
    });
    fixture = TestBed.createComponent(BooleanInputAttributeComponent);
    sut = fixture.componentInstance;
    dom = fixture.nativeElement;
  });

  it('creates instance of component', () => {
    expect(sut).not.toBeNull();
  });

  it('renders component', () => {
    expect(dom).not.toBeNull();
  });

  it('stores value set to fiz', () => {
    const value: boolean = true;
    expect(sut.fiz).not.toBe(value);
    sut.fiz = value;
    expect(sut.fiz).toBe(value);
  });

  it('initially have fizType set as empty string', () => {
    expect(sut.fizType).toBe('');
  });

  it('sets fizType to type of passed value', () => {
    const values: any[] = [true, false, 0, 333, null, undefined, '', 'test', new Date(), new MockType()];

    values.forEach((value) => {
      sut.fiz = value;
      expect(sut.fizType).toBe(typeof value);
    });
  });
});
