import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-boolean-input-attribute',
  templateUrl: 'BooleanInputAttributeComponent.html'
})
export class BooleanInputAttributeComponent {
  private _fiz: boolean;
  @Input()
  public get fiz(): boolean {
    return this._fiz;
  }
  public set fiz(value: boolean) {
    this._fiz = value;
    this.fizType = typeof value;
  }

  public fizType: string = '';
}
