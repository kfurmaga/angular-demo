import {ComponentFixture, TestBed} from '@angular/core/testing';
import {TestExamplesModule} from '@app/examples/TestExamplesModule';
import {CustomObjectInputAttributeComponent} from '@app/examples/components/custom-object-input-attribute/CustomObjectInputAttributeComponent';
import {CustomObject} from '@app/examples/model/CustomObject';

describe('CustomObjectInputAttributeComponent', () => {
  class MockType {}

  let fixture: ComponentFixture<CustomObjectInputAttributeComponent>;
  let sut: CustomObjectInputAttributeComponent;
  let dom: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestExamplesModule.forRoot()],
    });
    fixture = TestBed.createComponent(CustomObjectInputAttributeComponent);
    sut = fixture.componentInstance;
    dom = fixture.nativeElement;
  });

  it('creates instance of component', () => {
    expect(sut).not.toBeNull();
  });

  it('renders component', () => {
    expect(dom).not.toBeNull();
  });

  it('stores value set to baz', () => {
    const value: CustomObject = new CustomObject('333');
    expect(sut.baz).not.toBe(value);
    sut.baz = value;
    expect(sut.baz).toBe(value);
  });

  it('initially have bazType set as empty string', () => {
    expect(sut.bazType).toBe('');
  });

  it('sets bazType to type of passed value', () => {
    const values: any[] = [true, false, 0, 333, null, undefined, '', 'test', new Date(), new MockType()];

    values.forEach((value) => {
      sut.baz = value;
      expect(sut.bazType).toBe(typeof value);
    });
  });

  it('sets isCustomObject to true when instance of CustomObject is set as baz', () => {
    sut.baz = new CustomObject('333');
    expect(sut.isCustomObject).toBe(true);
  });

  it('sets isCustomObject to false when null is set as baz', () => {
    sut.baz = null;
    expect(sut.isCustomObject).toBe(false);
  });

  it('sets isCustomObject to false when other type is passed as baz', () => {
    const wrongCastedValue: any = 'test';
    sut.baz = wrongCastedValue;
    expect(sut.isCustomObject).toBe(false);
  });
});
