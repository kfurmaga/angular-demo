import {Component, Input} from '@angular/core';
import {CustomObject} from '@app/examples/model/CustomObject';

@Component({
  selector: 'app-custom-object-input-attribute',
  templateUrl: 'CustomObjectInputAttributeComponent.html'
})
export class CustomObjectInputAttributeComponent {
  private _baz: CustomObject;
  @Input()
  public get baz(): CustomObject {
    return this._baz;
  }
  public set baz(value: CustomObject) {
    this._baz = value;
    this.bazType = typeof value;
    this.isCustomObject = value instanceof CustomObject;
  }

  public bazType: string = '';

  public isCustomObject: boolean = false;
}
