import {Component} from '@angular/core';

@Component({
  selector: 'app-simple',
  templateUrl: 'SimpleComponent.html'
})
export class SimpleComponent {}
