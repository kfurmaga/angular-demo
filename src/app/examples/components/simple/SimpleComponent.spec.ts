import {ComponentFixture, TestBed} from '@angular/core/testing';
import {TestExamplesModule} from '@app/examples/TestExamplesModule';
import {SimpleComponent} from '@app/examples/components/simple/SimpleComponent';

describe('SimpleComponent', () => {
  let fixture: ComponentFixture<SimpleComponent>;
  let sut: SimpleComponent;
  let dom: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestExamplesModule.forRoot()],
    });
    fixture = TestBed.createComponent(SimpleComponent);
    sut = fixture.componentInstance;
    dom = fixture.nativeElement;
  });

  it('creates instance of component', () => {
    expect(sut).not.toBeNull();
  });

  it('renders component', () => {
    expect(dom).not.toBeNull();
  });
});
