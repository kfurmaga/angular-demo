import {ComponentFixture, TestBed} from '@angular/core/testing';
import {TestExamplesModule} from '@app/examples/TestExamplesModule';
import {TwoWayAttributeComponent} from '@app/examples/components/two-way-attribute/TwoWayAttributeComponent';
import createSpy = jasmine.createSpy;

describe('TwoWayAttributeComponent', () => {
  let fixture: ComponentFixture<TwoWayAttributeComponent>;
  let sut: TwoWayAttributeComponent;
  let dom: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestExamplesModule.forRoot()],
    });
    fixture = TestBed.createComponent(TwoWayAttributeComponent);
    sut = fixture.componentInstance;
    dom = fixture.nativeElement;
  });

  it('creates instance of component', () => {
    expect(sut).not.toBeNull();
  });

  it('renders component', () => {
    expect(dom).not.toBeNull();
  });

  describe('increase', () => {
    beforeEach(() => {
      sut.foo = 333;
    });

    it('increases foo', () => {
      sut.increase();
      expect(sut.foo).toBe(334);
    });

    it('dispatches fooChange Output', () => {
      const callback: (value: number) => void = createSpy();
      sut.fooChange.subscribe(callback);
      sut.increase();
      expect(callback).toHaveBeenCalledWith(334);
    });
  });
});
