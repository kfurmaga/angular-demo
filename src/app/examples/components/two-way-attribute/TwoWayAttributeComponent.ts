import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-two-way-attribute',
  templateUrl: 'TwoWayAttributeComponent.html'
})
export class TwoWayAttributeComponent {
  @Input()
  public foo: number;

  @Output()
  public fooChange: EventEmitter<number> = new EventEmitter();

  public increase(): void {
    this.foo++;
    this.fooChange.emit(this.foo);
  }
}
