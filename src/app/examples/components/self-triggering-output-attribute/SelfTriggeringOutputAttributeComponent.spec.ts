import {ComponentFixture, TestBed} from '@angular/core/testing';
import {TestExamplesModule} from '@app/examples/TestExamplesModule';
import createSpy = jasmine.createSpy;
import {SelfTriggeringOutputAttributeComponent} from '@app/examples/components/self-triggering-output-attribute/SelfTriggeringOutputAttributeComponent';
import {CustomObject} from '@app/examples/model/CustomObject';

describe('SelfTriggeringOutputAttributeComponent', () => {
  class MockType {}

  let fixture: ComponentFixture<SelfTriggeringOutputAttributeComponent>;
  let sut: SelfTriggeringOutputAttributeComponent;
  let dom: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestExamplesModule.forRoot()],
    });
    fixture = TestBed.createComponent(SelfTriggeringOutputAttributeComponent);
    sut = fixture.componentInstance;
    dom = fixture.nativeElement;
  });

  it('creates instance of component', () => {
    expect(sut).not.toBeNull();
  });

  it('renders component', () => {
    expect(dom).not.toBeNull();
  });

  describe('onClick', () => {
    it ('triggers baz output', () => {
      const callback: (customObject: CustomObject) => void = createSpy();
      sut.baz.subscribe(callback);
      sut.onClick(new MouseEvent('click'));
      expect(callback).toHaveBeenCalled();
    });
  });

  describe('baz', () => {
    it ('appends log when emmited', () => {
      const expectedLogsLength: number = sut.logs.length + 1;
      sut.baz.emit(new Date());
      expect(sut.logs.length).toBe(expectedLogsLength);
    });
  });

  describe('ngOnDestroy', () => {
    it ('disables event callbacks', () => {
      const expectedLogsLength: number = sut.logs.length;
      sut.ngOnDestroy();
      sut.baz.emit(new Date());
      expect(sut.logs.length).toBe(expectedLogsLength);
    });
  });
});
