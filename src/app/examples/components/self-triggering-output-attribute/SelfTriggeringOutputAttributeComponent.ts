import {Component, EventEmitter, OnDestroy, Output} from '@angular/core';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-self-triggering-output-attribute',
  templateUrl: 'SelfTriggeringOutputAttributeComponent.html',
})
export class SelfTriggeringOutputAttributeComponent implements OnDestroy {
  public logs: string[] = [];

  @Output()
  public baz: EventEmitter<Date> = new EventEmitter();

  private _bazSubscription: Subscription;

  public constructor() {
    this._bazSubscription = this.baz.subscribe(this.onBaz);
  }

  public onClick(event: MouseEvent): void {
    this.baz.emit(new Date());
  }

  public ngOnDestroy(): void {
    this._bazSubscription.unsubscribe();
  }

  private readonly onBaz = (date: Date): void => {
    this.logs.push(date.toISOString());
  };
}
