import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-string-input-attribute',
  templateUrl: 'StringInputAttributeComponent.html'
})
export class StringInputAttributeComponent {
  @Input()
  public foo: string;
}
