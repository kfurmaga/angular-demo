import {ComponentFixture, TestBed} from '@angular/core/testing';
import {TestExamplesModule} from '@app/examples/TestExamplesModule';
import {StringInputAttributeComponent} from '@app/examples/components/string-input-attribute/StringInputAttributeComponent';

describe('StringInputAttributeComponent', () => {
  let fixture: ComponentFixture<StringInputAttributeComponent>;
  let sut: StringInputAttributeComponent;
  let dom: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestExamplesModule.forRoot()],
    });
    fixture = TestBed.createComponent(StringInputAttributeComponent);
    sut = fixture.componentInstance;
    dom = fixture.nativeElement;
  });

  it('creates instance of component', () => {
    expect(sut).not.toBeNull();
  });

  it('renders component', () => {
    expect(dom).not.toBeNull();
  });

  // This kind of tests should only be written when business requires some values to be rendered or rendered in specific way
  it('renders value from foo attribute', () => {
    const value: string = 'MyTestValue';
    sut.foo = value;
    fixture.detectChanges();
    expect(dom.textContent.includes(value)).toBe(true);
  });
});
