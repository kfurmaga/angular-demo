export class ListItem {
  public constructor(public readonly title: string, public readonly excerpt: string) {}
}
