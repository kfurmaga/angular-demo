import {CustomObject} from '@app/examples/model/CustomObject';

describe('CustomObject', () => {
  it('creates instance', () => {
    const sut: CustomObject = new CustomObject('test');
    expect(sut).not.toBeNull();
  });
});
