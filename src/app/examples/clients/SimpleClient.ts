import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {MovieDto} from '@app/examples/dto/MovieDto';
import {Observable} from 'rxjs';

@Injectable()
export class SimpleClient {
  public constructor(private readonly _httpClient: HttpClient) {}

  public getLipsum(): Observable<MovieDto> {
    return this._httpClient.get<MovieDto>('/assets/movies/lipsum.json');
  }
}
