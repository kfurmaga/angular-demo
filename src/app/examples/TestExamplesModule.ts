import {ModuleWithProviders, NgModule} from '@angular/core';
import {ExamplesModule} from '@app/examples/ExamplesModule';
import {createProviders} from '@app/examples/injectables';

export class InjectablesMock {
  private _now: Date = new Date();

  public readonly interval: number = 333;

  private readonly _handlers: Function[] = [];

  public readonly getNow = (): Date => {
    return this._now;
  };

  public readonly setNow = (now: Date): void => {
    this._now = now;
    this.dispatchHandlers();
  };

  public readonly setInterval = (handler: Function, timeout: number): number => {
    this._handlers.push(handler);
    return 0;
  };

  public dispatchHandlers(): void {
    this._handlers.forEach((handler): void => handler());
  }
}

@NgModule({
  imports: [ExamplesModule],
})
export class TestExamplesModule {
  public static forRoot(): ModuleWithProviders {
    const injectables: InjectablesMock = new InjectablesMock();

    return {
      ngModule: ExamplesModule,
      providers: [
        ...createProviders(injectables.interval, injectables.getNow, injectables.setInterval),
        {
          provide: InjectablesMock,
          useValue: injectables,
        },
      ],
    };
  }
}
