import {CommonModule} from '@angular/common';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SimpleClient} from '@app/examples/clients/SimpleClient';
import {BooleanInputAttributeComponent} from '@app/examples/components/boolean-input-attribute/BooleanInputAttributeComponent';
import {CustomObjectInputAttributeComponent} from '@app/examples/components/custom-object-input-attribute/CustomObjectInputAttributeComponent';
import {CustomObjectOutputAttributeComponent} from '@app/examples/components/custom-object-output-attribute/CustomObjectOutputAttributeComponent';
import {NumberInputAttributeComponent} from '@app/examples/components/number-input-attribute/NumberInputAttributeComponent';
import {SelfTriggeringOutputAttributeComponent} from '@app/examples/components/self-triggering-output-attribute/SelfTriggeringOutputAttributeComponent';
import {SimpleInlineComponent} from '@app/examples/components/simple-inline/SimpleInlineComponent';
import {SimpleComponent} from '@app/examples/components/simple/SimpleComponent';
import {StringInputAttributeComponent} from '@app/examples/components/string-input-attribute/StringInputAttributeComponent';
import {TwoWayAttributeComponent} from '@app/examples/components/two-way-attribute/TwoWayAttributeComponent';
import {SimpleGuard} from '@app/examples/guards/SimpleGuard';
import {RequestInterceptor} from '@app/examples/interceptors/RequestInterceptor';
import {ResponseInterceptor} from '@app/examples/interceptors/ResponseInterceptor';
import {MovieDtoToListItemMapping} from '@app/examples/mappings/MovieDtoToListItemMapping';
import {MenuOutlet} from '@app/examples/outlets/MenuOutlet';
import {ClientsPage} from '@app/examples/pages/clients/ClientsPage';
import {ComponentsPage} from '@app/examples/pages/components/ComponentsPage';
import {DataAndHelpersPage} from '@app/examples/pages/data-and-helpers/DataAndHelpersPage';
import {GuardsPage} from '@app/examples/pages/guards/GuardsPage';
import {InjectablesPage} from '@app/examples/pages/injectables/InjectablesPage';
import {InputAttributesPage} from '@app/examples/pages/input-attributes/InputAttributesPage';
import {InterceptorsPage} from '@app/examples/pages/interceptors/InterceptorsPage';
import {MappingsPage} from '@app/examples/pages/mappings/MappingsPage';
import {OutputAttributesPage} from '@app/examples/pages/output-attributes/OutputAttributesPage';
import {ParametrizedResolversPage} from '@app/examples/pages/parametrized-resolvers/ParametrizedResolversPage';
import {PipesPage} from '@app/examples/pages/pipes/PipesPage';
import {ResolversPage} from '@app/examples/pages/resolvers/ResolversPage';
import {ServicesPage} from '@app/examples/pages/services/ServicesPage';
import {TwoWayAttributesPage} from '@app/examples/pages/two-way-attritbutes/TwoWayAttributesPage';
import {WrongUsageOfResolversPage} from '@app/examples/pages/wrong-usage-of-resolvers/WrongUsageOfResolversPage';
import {AgoPipe} from '@app/examples/pipes/AgoPipe';
import {InjectedServicePipe} from '@app/examples/pipes/InjectedServicePipe';
import {UppercasePipe} from '@app/examples/pipes/UppercasePipe';
import {ListItemResolver} from '@app/examples/resolvers/ListItemResolver';
import {RoarResolver} from '@app/examples/resolvers/RoarResolver';
import {ComposedService} from '@app/examples/services/ComposedService';
import {ListItemService} from '@app/examples/services/ListItemService';
import {ParallelService} from '@app/examples/services/ParallelService';
import {SimpleService} from '@app/examples/services/SimpleService';
import {StatefulService} from '@app/examples/services/StatefulService';

const routes: Routes = [
  {
    path: '',
    component: MenuOutlet,
    outlet: 'menu'
  },
  {
    path: 'components',
    component: ComponentsPage,
  },
  {
    path: 'pipes',
    component: PipesPage,
  },
  {
    path: 'input-attributes',
    component: InputAttributesPage,
  },
  {
    path: 'output-attributes',
    component: OutputAttributesPage,
  },
  {
    path: 'two-way-attributes',
    component: TwoWayAttributesPage,
  },
  {
    path: 'injectables',
    component: InjectablesPage,
    children: [
      {
        path: 'services',
        component: ServicesPage,
      },
      {
        path: 'data-and-helpers',
        component: DataAndHelpersPage,
      },
      {
        path: 'clients',
        component: ClientsPage,
      },
      {
        path: 'mappings',
        component: MappingsPage,
      },
      {
        path: 'resolvers',
        component: ResolversPage,
        resolve: {
          roar: RoarResolver,
        },
        children: [
          {
            path: 'parametrized/:itemId',
            component: ParametrizedResolversPage,
            resolve: {
              listItem: ListItemResolver,
            },
          },
          {
            path: 'wrong/:itemId',
            component: WrongUsageOfResolversPage,
            resolve: {
              listItem: ListItemResolver,
            },
          },
        ],
      },
      {
        path: 'guards',
        component: GuardsPage,
        canActivate: [SimpleGuard]
      },
      {
        path: 'interceptors',
        component: InterceptorsPage,
      }
    ],
  },
];

const components: any[] = [
  SimpleComponent,
  SimpleInlineComponent,
  StringInputAttributeComponent,
  NumberInputAttributeComponent,
  BooleanInputAttributeComponent,
  CustomObjectInputAttributeComponent,
  CustomObjectOutputAttributeComponent,
  SelfTriggeringOutputAttributeComponent,
  TwoWayAttributeComponent,
];

const interceptors: any[] = [
  {
    provide: HTTP_INTERCEPTORS,
    useClass: RequestInterceptor,
    multi: true,
  },
  {
    provide: HTTP_INTERCEPTORS,
    useClass: ResponseInterceptor,
    multi: true,
  },
];

const resolvers: any[] = [RoarResolver, ListItemResolver];

const mappings: any[] = [MovieDtoToListItemMapping];

const pages: any[] = [
  ComponentsPage,
  InputAttributesPage,
  OutputAttributesPage,
  TwoWayAttributesPage,
  InjectablesPage,
  ServicesPage,
  DataAndHelpersPage,
  ClientsPage,
  MappingsPage,
  ResolversPage,
  ParametrizedResolversPage,
  WrongUsageOfResolversPage,
  GuardsPage,
  InterceptorsPage,
  PipesPage
];

const pipes: any[] = [UppercasePipe, InjectedServicePipe, AgoPipe];
const outlets: any[] = [MenuOutlet];
const guards: any[] = [SimpleGuard];
const clients: any[] = [SimpleClient];
const services: any[] = [SimpleService, ComposedService, StatefulService, ParallelService, ListItemService];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes), HttpClientModule],
  exports: [...components, ...outlets, ...pages, ...pipes],
  declarations: [...components, ...outlets, ...pages, ...pipes],
  providers: [...clients, ...services, ...mappings, ...resolvers, ...guards, ...interceptors, ...pipes],
})
export class ExamplesModule {}
