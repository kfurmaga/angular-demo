import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';
import {TestExamplesModule} from '@app/examples/TestExamplesModule';
import {MovieDto} from '@app/examples/dto/MovieDto';
import {ListItemResolver} from '@app/examples/resolvers/ListItemResolver';
import {MockActivatedRoute} from '@app/shared/testing/MockActivatedRoute';

describe('ListItemResolver', () => {
  let sut: ListItemResolver;
  let activatedRoute: MockActivatedRoute;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, TestExamplesModule.forRoot()],
    });
    httpMock = TestBed.get(HttpTestingController);
    activatedRoute = new MockActivatedRoute({}, {itemId: 333});
    sut = TestBed.get(ListItemResolver);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('creates instance of resolver', () => {
    expect(sut).not.toBeNull();
  });

  describe('resolve', () => {
    it("doesn't require state param", () => {
      expect(() => {
        sut.resolve(activatedRoute.snapshot, null);
      }).not.toThrow();
    });

    it('returns not null filled list item', (done: DoneFn) => {
      sut.resolve(activatedRoute.snapshot, null).subscribe((result) => {
        expect(result).not.toBeNull();
        expect(result.title).not.toBeNull();
        expect(result.excerpt).not.toBeNull();
        done();
      }, () => done.fail());

      httpMock.expectOne(`/assets/movies/lipsum.json`).flush(MovieDto.createMock(1));
    });
  });
});
