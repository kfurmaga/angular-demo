import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';

@Injectable()
export class RoarResolver implements Resolve<string> {
  public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): string {
    return 'Wrrrrrr......';
  }
}
