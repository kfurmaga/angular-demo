import {TestBed} from '@angular/core/testing';
import {TestExamplesModule} from '@app/examples/TestExamplesModule';
import {RoarResolver} from '@app/examples/resolvers/RoarResolver';
import {MockActivatedRoute} from '@app/shared/testing/MockActivatedRoute';

describe('RoarResolver', () => {
  let sut: RoarResolver;
  let activatedRoute: MockActivatedRoute;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestExamplesModule.forRoot()],
    });
    activatedRoute = new MockActivatedRoute();
    sut = TestBed.get(RoarResolver);
  });

  it('creates instance of resolver', () => {
    expect(sut).not.toBeNull();
  });

  describe('resolve', () => {
    it("doesn't require state param", () => {
      expect(() => {
        sut.resolve(activatedRoute.snapshot, null);
      }).not.toThrow();
    });

    it('returns not empty string', () => {
      const result: string = sut.resolve(activatedRoute.snapshot, null);
      expect(result).not.toBe('');
      expect(result).not.toBeNull();
    });
  });
});
