import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {ListItem} from '@app/examples/model/ListItem';
import {getItemId} from '@app/examples/selectors/params';
import {ListItemService} from '@app/examples/services/ListItemService';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable()
export class ListItemResolver implements Resolve<ListItem> {
  public constructor(private readonly _listItemService: ListItemService) {}

  public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ListItem> {
    const itemId: number = getItemId(route.params);
    return this._listItemService.getById(itemId).take(1);
  }
}
