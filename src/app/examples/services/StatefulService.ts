import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable()
export class StatefulService {
  private _now: BehaviorSubject<Date> = new BehaviorSubject(new Date());
  public readonly now: Observable<Date> = this._now;

  public updateNow(): void {
    this._now.next(new Date());
  }
}
