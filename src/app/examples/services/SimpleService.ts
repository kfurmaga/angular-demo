import {Injectable} from '@angular/core';

@Injectable()
export class SimpleService {
  public readonly bar: string = "I'm so simple";
}
