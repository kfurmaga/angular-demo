import {ComponentFixture, TestBed} from '@angular/core/testing';
import {TestExamplesModule} from '@app/examples/TestExamplesModule';
import {SimpleService} from '@app/examples/services/SimpleService';

describe('SimpleService', () => {
  let sut: SimpleService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestExamplesModule.forRoot()],
    });
    sut = TestBed.get(SimpleService);
  });

  it('creates instance of component', () => {
    expect(sut).not.toBeNull();
  });

  describe('bar', () => {
    it('is not null or empty', () => {
      expect(sut.bar).not.toBeNull(sut.bar);
      expect(sut.bar).not.toBe('');
    });
  });
});
