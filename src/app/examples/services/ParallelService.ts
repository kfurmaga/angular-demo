import {Inject, Injectable} from '@angular/core';
import {GET_NOW, INTERVAL, SET_INTERVAL} from '@app/examples/injectables';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable()
export class ParallelService {
  private readonly _now: BehaviorSubject<Date> = new BehaviorSubject(this._getNow());
  public readonly now: Observable<Date> = this._now;

  public constructor(
    @Inject(INTERVAL) interval: number,
    @Inject(SET_INTERVAL) setInterval: (handler: TimerHandler, timeout: number) => number,
    @Inject(GET_NOW) private readonly _getNow: () => Date
  ) {
    setInterval(this.updateNow, interval);
  }

  private readonly updateNow = (): void => {
    this._now.next(this._getNow());
  };
}
