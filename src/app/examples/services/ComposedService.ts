import {Injectable} from '@angular/core';
import {SimpleService} from '@app/examples/services/SimpleService';

@Injectable()
export class ComposedService {
  public readonly foo: string;

  public constructor(simpleService: SimpleService) {
    this.foo = simpleService.bar.toUpperCase();
  }
}
