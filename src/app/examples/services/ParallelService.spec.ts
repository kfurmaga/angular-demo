import {ComponentFixture, TestBed} from '@angular/core/testing';
import {InjectablesMock, TestExamplesModule} from '@app/examples/TestExamplesModule';
import {ParallelService} from '@app/examples/services/ParallelService';
import createSpy = jasmine.createSpy;

describe('ParallelService', () => {
  let sut: ParallelService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestExamplesModule.forRoot()],
    });
    sut = TestBed.get(ParallelService);
  });

  it('creates instance of component', () => {
    expect(sut).not.toBeNull();
  });

  describe('now', () => {
    it ('calls subscription immediately', () => {
      const callback: (date: Date) => void = createSpy();
      sut.now.subscribe(callback);
      expect(callback).toHaveBeenCalledTimes(1);
    });

    it ('current value is not null', () => {
      sut.now.subscribe((now) => {
        expect(now).not.toBeNull();
      });
    });

    it ('gets updated every time that setInterval callback is called.', () => {
      const injectables: InjectablesMock = TestBed.get(InjectablesMock);
      const callback: (date: Date) => void = createSpy();
      const dates: Date[] = [
        new Date(2019, 3, 29, 9, 34, 10),
        new Date(2019, 3, 29, 9, 34, 11),
        new Date(2019, 3, 29, 9, 34, 12),
        new Date(2019, 3, 29, 9, 34, 13),
      ];
      sut.now.skip(1).subscribe(callback);

      dates.forEach(date => {
        injectables.setNow(date);
        expect(callback).toHaveBeenCalledWith(date);
      });
    });
  });
});
