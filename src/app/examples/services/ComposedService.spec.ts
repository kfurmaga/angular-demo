import {ComponentFixture, TestBed} from '@angular/core/testing';
import {TestExamplesModule} from '@app/examples/TestExamplesModule';
import {ComposedService} from '@app/examples/services/ComposedService';

describe('ComposedService', () => {
  let sut: ComposedService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestExamplesModule.forRoot()],
    });
    sut = TestBed.get(ComposedService);
  });

  it('creates instance of component', () => {
    expect(sut).not.toBeNull();
  });

  describe('foo', () => {
    it('is not null or empty', () => {
      expect(sut.foo).not.toBeNull(sut.foo);
      expect(sut.foo).not.toBe('');
    });
  });
});
