import {TestBed} from '@angular/core/testing';
import {TestExamplesModule} from '@app/examples/TestExamplesModule';
import {StatefulService} from '@app/examples/services/StatefulService';
import createSpy = jasmine.createSpy;

describe('StatefulService', () => {
  let sut: StatefulService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestExamplesModule.forRoot()],
    });
    sut = TestBed.get(StatefulService);
  });

  it('creates instance of component', () => {
    expect(sut).not.toBeNull();
  });

  describe('now', () => {
    it ('calls subscription immediately', () => {
      const callback: (date: Date) => void = createSpy();
      sut.now.subscribe(callback);
      expect(callback).toHaveBeenCalledTimes(1);
    });

    it ('current value is not null', () => {
      sut.now.subscribe((now) => {
        expect(now).not.toBeNull();
      });
    });
  });

  describe('updateNow', () => {
    it ('calls subscription with new value', () => {
      const callback: (date: Date) => void = createSpy();
      sut.now.skip(1).subscribe(callback);
      sut.updateNow();
      expect(callback).toHaveBeenCalledTimes(1);
    });
  });
});
