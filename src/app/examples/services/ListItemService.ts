import {Injectable} from '@angular/core';
import {SimpleClient} from '@app/examples/clients/SimpleClient';
import {MovieDto} from '@app/examples/dto/MovieDto';
import {MovieDtoToListItemMapping} from '@app/examples/mappings/MovieDtoToListItemMapping';
import {ListItem} from '@app/examples/model/ListItem';
import {Observable} from 'rxjs';

@Injectable()
export class ListItemService {
  public constructor(
    private readonly _simpleClient: SimpleClient,
    private readonly _toListItem: MovieDtoToListItemMapping
  ) {}

  public getById(id: number): Observable<ListItem> {
    return this._simpleClient
      .getLipsum()
      .map(this.prependWithId(id))
      .mergeMap(this._toListItem.convert);
  }

  private readonly prependWithId = (id: number): ((dto: MovieDto) => MovieDto) => {
    return (dto: MovieDto): MovieDto => ({
      title: `${id}. ${dto.title}`,
      description: `${id} ${dto.description}`,
    });
  };
}
