import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {of, Observable} from 'rxjs';

@Injectable()
export class ResponseInterceptor implements HttpInterceptor {
  public static readonly INTERCEPTED_URL: string = '/interception-example';
  public static readonly INTERCEPTED_VALUE: string = 'Intercepted value';

  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.url !== ResponseInterceptor.INTERCEPTED_URL) {
      return next.handle(req);
    }

    return next
      .handle(req)
      .map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          return event.clone({body: ResponseInterceptor.INTERCEPTED_VALUE});
        }

        return event;
      })
      .catch((error: HttpErrorResponse) => {
        return of(
          new HttpResponse({
            body: ResponseInterceptor.INTERCEPTED_VALUE,
            headers: req.headers,
            status: 200,
          })
        );
      });
  }
}
