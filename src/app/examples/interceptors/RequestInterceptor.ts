import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

@Injectable()
export class RequestInterceptor implements HttpInterceptor {
  public static readonly INTERCEPTED_HEADER: string = 'crocodile';
  public static readonly INTERCEPTED_VALUE: string = 'small green dragon';

  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const newRequest: HttpRequest<any> = req.clone({
      setHeaders: {
        [RequestInterceptor.INTERCEPTED_HEADER]: RequestInterceptor.INTERCEPTED_VALUE,
      }
    });
    return next.handle(newRequest);
  }
}
