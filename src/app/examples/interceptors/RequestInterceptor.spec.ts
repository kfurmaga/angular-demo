import {HttpClient} from '@angular/common/http';
import {HttpClientTestingModule, HttpTestingController, TestRequest} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';
import {TestExamplesModule} from '@app/examples/TestExamplesModule';
import {RequestInterceptor} from '@app/examples/interceptors/RequestInterceptor';

describe('RequestInterceptor', () => {
  const expectedResponse: string = 'expected response';
  let httpMock: HttpTestingController;
  let httpClient: HttpClient;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestExamplesModule.forRoot(), HttpClientTestingModule],
    });
    httpMock = TestBed.get(HttpTestingController);
    httpClient = TestBed.get(HttpClient);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('appends header with intercepted field and value', (done: DoneFn) => {
    httpClient.get('/test').subscribe(
      (response) => {
        expect(response).toBeTruthy();
        done();
      },
      () => done.fail()
    );

    const httpRequest: TestRequest = httpMock.expectOne(`/test`);

    expect(httpRequest.request.headers.has(RequestInterceptor.INTERCEPTED_HEADER)).toEqual(true);
    expect(httpRequest.request.headers.get(RequestInterceptor.INTERCEPTED_HEADER)).toEqual(
      RequestInterceptor.INTERCEPTED_VALUE
    );

    httpRequest.flush(expectedResponse);
  });

  it("doesn't remove existing header values", (done: DoneFn) => {
    const headers: {[header: string]: string} = {
      trololo: 'ha ha ha ha ha',
    };
    httpClient.get('/test', {headers}).subscribe(
      (response) => {
        expect(response).toBeTruthy();
        done();
      },
      () => done.fail()
    );

    const httpRequest: TestRequest = httpMock.expectOne(`/test`);

    expect(httpRequest.request.headers.has('trololo')).toEqual(true);
    expect(httpRequest.request.headers.get('trololo')).toEqual(headers.trololo);

    httpRequest.flush(expectedResponse);
  });
});
