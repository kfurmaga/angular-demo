import {HttpClient} from '@angular/common/http';
import {HttpClientTestingModule, HttpTestingController, TestRequest} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';
import {TestExamplesModule} from '@app/examples/TestExamplesModule';
import {ResponseInterceptor} from '@app/examples/interceptors/ResponseInterceptor';

describe('ResponseInterceptor', () => {
  const expectedResponse: string = 'Quick brown fox jumped over lazy dog.';
  let httpMock: HttpTestingController;
  let httpClient: HttpClient;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestExamplesModule.forRoot(), HttpClientTestingModule],
    });
    httpMock = TestBed.get(HttpTestingController);
    httpClient = TestBed.get(HttpClient);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('returns intercepted value for ResponseInterceptor.INTERCEPTED_URL', (done: DoneFn) => {
    httpClient.get(ResponseInterceptor.INTERCEPTED_URL).subscribe(
      (response) => {
        expect(response).toBe(ResponseInterceptor.INTERCEPTED_VALUE);
        done();
      },
      () => done.fail()
    );

    httpMock.expectOne(ResponseInterceptor.INTERCEPTED_URL).flush(expectedResponse);
  });

  it('returns response with code 200 when request to ResponseInterceptor.INTERCEPTED_URL fails', (done: DoneFn) => {
    httpClient.get(ResponseInterceptor.INTERCEPTED_URL).subscribe(
      (response) => {
        expect(response).toBe(ResponseInterceptor.INTERCEPTED_VALUE);
        done();
      },
      () => done.fail()
    );

    httpMock.expectOne(ResponseInterceptor.INTERCEPTED_URL).error(new ErrorEvent('something went wrong'));
  });

  it('returns request value for url other than ResponseInterceptor.INTERCEPTED_URL', (done: DoneFn) => {
    const url: string = '/test';

    expect(url).not.toBe(ResponseInterceptor.INTERCEPTED_URL);
    expect(expectedResponse).not.toBe(ResponseInterceptor.INTERCEPTED_VALUE);

    httpClient.get(url).subscribe(
      (response) => {
        expect(response).toBe(expectedResponse);
        done();
      },
      () => done.fail()
    );

    httpMock.expectOne(`/test`).flush(expectedResponse);
  });
});
