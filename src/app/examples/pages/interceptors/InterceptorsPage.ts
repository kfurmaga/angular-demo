import {HttpClient} from '@angular/common/http';
import {Component} from '@angular/core';
import {ResponseInterceptor} from '@app/examples/interceptors/ResponseInterceptor';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-interceptors-page',
  templateUrl: 'InterceptorsPage.html'
})
export class InterceptorsPage {
  public readonly tiger: Observable<string> = this._httpClient.get<string>(ResponseInterceptor.INTERCEPTED_URL);

  public constructor(private readonly _httpClient: HttpClient) {
  }
}
