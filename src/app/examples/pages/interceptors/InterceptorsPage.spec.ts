import {ComponentFixture, TestBed} from '@angular/core/testing';
import {TestExamplesModule} from '@app/examples/TestExamplesModule';
import {InterceptorsPage} from '@app/examples/pages/interceptors/InterceptorsPage';

describe('InterceptorsPage', () => {
  let fixture: ComponentFixture<InterceptorsPage>;
  let sut: InterceptorsPage;
  let dom: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestExamplesModule.forRoot()],
    });
    fixture = TestBed.createComponent(InterceptorsPage);
    sut = fixture.componentInstance;
    dom = fixture.nativeElement;
  });

  it('creates instance of page', () => {
    expect(sut).not.toBeNull();
  });

  it('renders page', () => {
    expect(dom).not.toBeNull();
  });
});
