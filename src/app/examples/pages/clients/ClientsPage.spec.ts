import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {TestExamplesModule} from '@app/examples/TestExamplesModule';
import {MovieDto} from '@app/examples/dto/MovieDto';
import {ClientsPage} from '@app/examples/pages/clients/ClientsPage';
import createSpy = jasmine.createSpy;

describe('ClientsPage', () => {
  let fixture: ComponentFixture<ClientsPage>;
  let sut: ClientsPage;
  let dom: HTMLElement;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, TestExamplesModule.forRoot()],
    });
    fixture = TestBed.createComponent(ClientsPage);
    httpMock = TestBed.get(HttpTestingController);
    sut = fixture.componentInstance;
    dom = fixture.nativeElement;
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('creates instance of page', () => {
    expect(sut).not.toBeNull();
  });

  it('renders page', () => {
    expect(dom).not.toBeNull();
  });

  describe('lipsum', () => {
    it('has same value as returned from API', () => {
      const expectedLispum: MovieDto = MovieDto.createMock(333);
      const callback: (dto: MovieDto) => void = createSpy();
      sut.lipsum.subscribe(callback);

      httpMock.expectOne(`/assets/movies/lipsum.json`).flush(expectedLispum);

      expect(callback).toHaveBeenCalledWith(expectedLispum);
    });
  });
});
