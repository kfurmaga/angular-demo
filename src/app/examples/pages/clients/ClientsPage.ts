import {Component} from '@angular/core';
import {SimpleClient} from '@app/examples/clients/SimpleClient';
import {MovieDto} from '@app/examples/dto/MovieDto';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-clients-page',
  templateUrl: 'ClientsPage.html'
})
export class ClientsPage {
  public readonly lipsum: Observable<MovieDto>;

  public constructor(simpleClient: SimpleClient) {
    this.lipsum = simpleClient.getLipsum();
  }
}
