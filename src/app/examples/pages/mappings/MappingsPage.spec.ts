import {ComponentFixture, TestBed} from '@angular/core/testing';
import {TestExamplesModule} from '@app/examples/TestExamplesModule';
import {MappingsPage} from '@app/examples/pages/mappings/MappingsPage';

describe('MappingsPage', () => {
  let fixture: ComponentFixture<MappingsPage>;
  let sut: MappingsPage;
  let dom: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestExamplesModule.forRoot()],
    });
    fixture = TestBed.createComponent(MappingsPage);
    sut = fixture.componentInstance;
    dom = fixture.nativeElement;
  });

  it('creates instance of page', () => {
    expect(sut).not.toBeNull();
  });

  it('renders page', () => {
    expect(dom).not.toBeNull();
  });

  describe('listItem', () => {
    it('is not null', (done: DoneFn) => {
      sut.listItem.subscribe((listItem) => {
        expect(listItem).not.toBeNull();
        done();
      });
    });
  });
});
