import {Component} from '@angular/core';
import {MovieDto} from '@app/examples/dto/MovieDto';
import {MovieDtoToListItemMapping} from '@app/examples/mappings/MovieDtoToListItemMapping';
import {ListItem} from '@app/examples/model/ListItem';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-mappings-page',
  templateUrl: 'MappingsPage.html'
})
export class MappingsPage {
  public readonly listItem: Observable<ListItem>;

  public constructor(
    toListItem: MovieDtoToListItemMapping
  ) {
    this.listItem = toListItem.convert(MovieDto.createMock(333));
  }
}
