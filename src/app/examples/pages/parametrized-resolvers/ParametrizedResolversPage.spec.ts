import {ComponentFixture, TestBed} from '@angular/core/testing';
import {ActivatedRoute} from '@angular/router';
import {RouterTestingModule} from '@angular/router/testing';
import {TestExamplesModule} from '@app/examples/TestExamplesModule';
import {ListItem} from '@app/examples/model/ListItem';
import {ParametrizedResolversPage} from '@app/examples/pages/parametrized-resolvers/ParametrizedResolversPage';
import {MockActivatedRoute} from '@app/shared/testing/MockActivatedRoute';
import createSpy = jasmine.createSpy;

describe('ParametrizedResolversPage', () => {
  const itemId: number = 1;
  const resolvedRoar: string = 'Aggrrr...';
  const resolvedListItem: ListItem = new ListItem('resolved title', 'resolved excerpt');
  let fixture: ComponentFixture<ParametrizedResolversPage>;
  let sut: ParametrizedResolversPage;
  let dom: HTMLElement;
  let activatedRoute: MockActivatedRoute;

  beforeEach(() => {
    activatedRoute = new MockActivatedRoute({roar: resolvedRoar, listItem: resolvedListItem}, {itemId});

    TestBed.configureTestingModule({
      imports: [TestExamplesModule.forRoot(), RouterTestingModule],
      providers: [{provide: ActivatedRoute, useValue: activatedRoute}],
    });
    fixture = TestBed.createComponent(ParametrizedResolversPage);
    sut = fixture.componentInstance;
    dom = fixture.nativeElement;
  });

  it('creates instance of page', () => {
    expect(sut).not.toBeNull();
  });

  it('renders page', () => {
    expect(dom).not.toBeNull();
  });

  describe('title', () => {
    it('initially contains resolvedListItem.title', (done: DoneFn) => {
      sut.title.subscribe(
        (title) => {
          expect(title).toEqual(resolvedListItem.title);
          done();
        },
        () => done.fail()
      );
    });

    it('updates when activatedRoute.data.listItem was changed', () => {
      const updatedListItem: ListItem = new ListItem('updated title', 'updated excerpt');
      const callback: (title: string) => void = createSpy();

      sut.title.subscribe(callback);

      activatedRoute.updateData('listItem', updatedListItem);

      expect(callback).toHaveBeenCalledWith(updatedListItem.title);
    });
  });

  describe('itemId', () => {
    it('initially contains itemId', (done: DoneFn) => {
      sut.itemId.subscribe(
        (value) => {
          expect(value).toEqual(itemId);
          done();
        },
        () => done.fail()
      );
    });

    it('updates when activatedRoute.param.itemId was changed', () => {
      const updatedItemId: number = 333;
      const callback: (itemId: number) => void = createSpy();

      sut.itemId.subscribe(callback);

      activatedRoute.updateParam('itemId', updatedItemId);

      expect(callback).toHaveBeenCalledWith(updatedItemId);
    });
  });

  describe('excerpt', () => {
    it('initially contains resolvedListItem.excerpt', (done: DoneFn) => {
      sut.excerpt.subscribe(
        (excerpt) => {
          expect(excerpt).toEqual(resolvedListItem.excerpt);
          done();
        },
        () => done.fail()
      );
    });

    it('updates when activatedRoute.data.listItem was changed', () => {
      const updatedListItem: ListItem = new ListItem('updated title', 'updated excerpt');
      const callback: (excerpt: string) => void = createSpy();

      sut.excerpt.subscribe(callback);

      activatedRoute.updateData('listItem', updatedListItem);

      expect(callback).toHaveBeenCalledWith(updatedListItem.excerpt);
    });
  });

  describe('roar', () => {
    it('initially contains resolvedRoar', (done: DoneFn) => {
      sut.roar.subscribe(
        (roar) => {
          expect(roar).toEqual(resolvedRoar);
          done();
        },
        () => done.fail()
      );
    });

    it('updates when activatedRoute.data.roar was changed', () => {
      const updatedRoar: string = 'updated roar';
      const callback: (title: string) => void = createSpy();

      sut.roar.subscribe(callback);

      activatedRoute.updateData('roar', updatedRoar);

      expect(callback).toHaveBeenCalledWith(updatedRoar);
    });
  });
});
