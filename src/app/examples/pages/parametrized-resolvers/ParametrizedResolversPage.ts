import {Component} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ListItem} from '@app/examples/model/ListItem';
import {getListItem, getRoar} from '@app/examples/selectors/data';
import {getItemId} from '@app/examples/selectors/params';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-parametrized-resolver-page',
  templateUrl: 'ParametrizedResolversPage.html'
})
export class ParametrizedResolversPage {
  private readonly _listItem: Observable<ListItem> = this._activatedRoute.data.map(getListItem);

  public itemId: Observable<number> = this._activatedRoute.params.map(getItemId);
  public title: Observable<string> = this._listItem.map(listItem => listItem.title);
  public excerpt: Observable<string> = this._listItem.map(listItem => listItem.excerpt);
  public roar: Observable<string> = this._activatedRoute.data.map(getRoar);

  public constructor(private readonly _activatedRoute: ActivatedRoute) {
  }
}
