import {ComponentFixture, TestBed} from '@angular/core/testing';
import {ActivatedRoute} from '@angular/router';
import {RouterTestingModule} from '@angular/router/testing';
import {TestExamplesModule} from '@app/examples/TestExamplesModule';
import {ResolversPage} from '@app/examples/pages/resolvers/ResolversPage';
import {MockActivatedRoute} from '@app/shared/testing/MockActivatedRoute';

describe('ResolversPage', () => {
  const resolvedRoar: string = 'Aggrrr...';
  let fixture: ComponentFixture<ResolversPage>;
  let sut: ResolversPage;
  let dom: HTMLElement;
  let activatedRoute: MockActivatedRoute;

  beforeEach(() => {
    activatedRoute = new MockActivatedRoute({roar: resolvedRoar});

    TestBed.configureTestingModule({
      imports: [TestExamplesModule.forRoot(), RouterTestingModule],
      providers: [{provide: ActivatedRoute, useValue: activatedRoute}],
    });
    fixture = TestBed.createComponent(ResolversPage);
    sut = fixture.componentInstance;
    dom = fixture.nativeElement;
  });

  it('creates instance of page', () => {
    expect(sut).not.toBeNull();
  });

  it('renders page', () => {
    expect(dom).not.toBeNull();
  });

  describe('zonk', () => {
    it('initially contains resolvedRoar', (done: DoneFn) => {
      sut.zonk.subscribe(
        (zonk) => {
          expect(zonk).toEqual(resolvedRoar);
          done();
        },
        () => done.fail()
      );
    });
  });
});
