import {Component} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {getRoar} from '@app/examples/selectors/data';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-resolvers-page',
  templateUrl: 'ResolversPage.html'
})
export class ResolversPage {
  public zonk: Observable<string>;

  public constructor(activatedRoute: ActivatedRoute) {
    this.zonk = activatedRoute.data.map(getRoar);
  }
}
