import {Component} from '@angular/core';

@Component({
  selector: 'app-two-way-databinding-page',
  templateUrl: 'TwoWayAttributesPage.html'
})
export class TwoWayAttributesPage {
  public counter: number = 0;

  public reset(): void {
    this.counter = 0;
  }

  public onFooChange(foo: number): void {
    this.counter = foo;
  }
}
