import {ComponentFixture, TestBed} from '@angular/core/testing';
import {TestExamplesModule} from '@app/examples/TestExamplesModule';
import {TwoWayAttributesPage} from '@app/examples/pages/two-way-attritbutes/TwoWayAttributesPage';

describe('TwoWayAttributesPage', () => {
  let fixture: ComponentFixture<TwoWayAttributesPage>;
  let sut: TwoWayAttributesPage;
  let dom: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestExamplesModule.forRoot()],
    });
    fixture = TestBed.createComponent(TwoWayAttributesPage);
    sut = fixture.componentInstance;
    dom = fixture.nativeElement;
  });

  it('creates instance of page', () => {
    expect(sut).not.toBeNull();
  });

  it('renders page', () => {
    expect(dom).not.toBeNull();
  });

  describe('onFooChange', () => {
    it('updates counter', () => {
      const expectedCounter: number = 333;
      sut.onFooChange(expectedCounter)
      expect(sut.counter).toBe(expectedCounter);
    });
  });

  describe('reset', () => {
    it('sets counter to 0', () => {
      sut.counter = 333;
      sut.reset();
      expect(sut.counter).toBe(0);
    });
  });
});
