import {Component, Input} from '@angular/core';
import {CustomObject} from '@app/examples/model/CustomObject';

@Component({
  selector: 'app-input-attributes-page',
  templateUrl: 'InputAttributesPage.html'
})
export class InputAttributesPage {
  public nation: string = 'Polish';
  public isHappy: boolean = true;
  public age: number = 333;
  public first: CustomObject = new CustomObject('one');

  public getOtherNation(): string {
    return 'Spanish';
  }

  public isNotHappy(): boolean {
    return !this.isHappy;
  }

  public getTripleAge(): number {
    return 3 * this.age;
  }

  public getSecond(): CustomObject {
    return new CustomObject('two');
  }
}
