import {ComponentFixture, TestBed} from '@angular/core/testing';
import {TestExamplesModule} from '@app/examples/TestExamplesModule';
import {CustomObject} from '@app/examples/model/CustomObject';
import {InputAttributesPage} from '@app/examples/pages/input-attributes/InputAttributesPage';

describe('InputAttributesPage', () => {
  let fixture: ComponentFixture<InputAttributesPage>;
  let sut: InputAttributesPage;
  let dom: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestExamplesModule.forRoot()],
    });
    fixture = TestBed.createComponent(InputAttributesPage);
    sut = fixture.componentInstance;
    dom = fixture.nativeElement;
  });

  it('creates instance of page', () => {
    expect(sut).not.toBeNull();
  });

  it('renders page', () => {
    expect(dom).not.toBeNull();
  });

  describe('isNotHappy', () => {
    it('returns false when isHappy is set to true', () => {
      sut.isHappy = true;
      expect(sut.isNotHappy()).toBe(false);
    });

    it ('returns true when isHappy is set to false', () => {
      sut.isHappy = false;
      expect(sut.isNotHappy()).toBe(true);
    });
  });

  describe('getTripleAge', () => {
    it('returns NaN when age is NaN', () => {
      sut.age = NaN;
      expect(sut.getTripleAge()).toBeNaN();
    });

    it('returns 9 when age is 3', () => {
      sut.age = 3;
      expect(sut.getTripleAge()).toBe(9);
    });

    it('returns -333 when age is -111', () => {
      sut.age = -111;
      expect(sut.getTripleAge()).toBe(-333);
    });

    it('returns 0 when age is 0', () => {
      sut.age = 0;
      expect(sut.getTripleAge()).toBe(0);
    });
  });

  describe('getSecond', () => {
    it ('returns instance of CustomObject', () => {
      const result: CustomObject = sut.getSecond();
      expect(result).not.toBeNull();
      expect(result instanceof CustomObject).toBe(true);
    });
  });

  describe('getOtherNation', () => {
    it ('doesn\'t return null', () => {
      const result: string = sut.getOtherNation();
      expect(result).not.toBeNull();
    });

    it ('doesn\'t return empty string', () => {
      const result: string = sut.getOtherNation();
      expect(result).not.toBe('');
    });
  });
});
