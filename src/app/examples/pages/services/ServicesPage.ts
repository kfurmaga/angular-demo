import {Component} from '@angular/core';
import {ComposedService} from '@app/examples/services/ComposedService';
import {ParallelService} from '@app/examples/services/ParallelService';
import {SimpleService} from '@app/examples/services/SimpleService';
import {StatefulService} from '@app/examples/services/StatefulService';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-services-page',
  templateUrl: 'ServicesPage.html',
})
export class ServicesPage {
  public simpleServiceBar: string;
  public composedServiceFoo: string;
  public now: Observable<Date>;
  public currentTime: Observable<Date>;

  public constructor(
    private readonly _statefulService: StatefulService,
    simpleService: SimpleService,
    composedService: ComposedService,
    parallelService: ParallelService
  ) {
    this.simpleServiceBar = simpleService.bar;
    this.composedServiceFoo = composedService.foo;
    this.now = this._statefulService.now;
    this.currentTime = parallelService.now;
  }

  public updateNow(): void {
    this._statefulService.updateNow();
  }
}
