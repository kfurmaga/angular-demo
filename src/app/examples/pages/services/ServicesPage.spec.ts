import {ComponentFixture, TestBed} from '@angular/core/testing';
import {TestExamplesModule} from '@app/examples/TestExamplesModule';
import {ServicesPage} from '@app/examples/pages/services/ServicesPage';
import createSpy = jasmine.createSpy;

describe('ServicesPage', () => {
  let fixture: ComponentFixture<ServicesPage>;
  let sut: ServicesPage;
  let dom: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestExamplesModule.forRoot()],
    });
    fixture = TestBed.createComponent(ServicesPage);
    sut = fixture.componentInstance;
    dom = fixture.nativeElement;
  });

  it('creates instance of page', () => {
    expect(sut).not.toBeNull();
  });

  it('renders page', () => {
    expect(dom).not.toBeNull();
  });

  describe('updateNow', () => {
    it('changes now value', () => {
      const callback: (now: Date) => void = createSpy();
      sut.now.subscribe(callback);
      sut.updateNow();
      expect(callback).toHaveBeenCalled();
    });
  });
});
