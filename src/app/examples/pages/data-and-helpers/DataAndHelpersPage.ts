import {Component, Inject} from '@angular/core';
import {GET_NOW, INTERVAL} from '@app/examples/injectables';

@Component({
  selector: 'app-data-and-helpers-page',
  templateUrl: 'DataAndHelpersPage.html'
})
export class DataAndHelpersPage {
  public readonly now: Date;

  public constructor(
    @Inject(INTERVAL) public readonly interval: number,
    @Inject(GET_NOW) getNow: () => Date
  ) {
    this.now = getNow();
  }
}
