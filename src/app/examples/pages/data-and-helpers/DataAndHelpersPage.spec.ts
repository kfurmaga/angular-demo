import {ComponentFixture, TestBed} from '@angular/core/testing';
import {InjectablesMock, TestExamplesModule} from '@app/examples/TestExamplesModule';
import {DataAndHelpersPage} from '@app/examples/pages/data-and-helpers/DataAndHelpersPage';

  describe('DataAndHelpersPage', () => {
  let fixture: ComponentFixture<DataAndHelpersPage>;
  let sut: DataAndHelpersPage;
  let dom: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestExamplesModule.forRoot()],
    });
    fixture = TestBed.createComponent(DataAndHelpersPage);
    sut = fixture.componentInstance;
    dom = fixture.nativeElement;
  });

  it('creates instance of page', () => {
    expect(sut).not.toBeNull();
  });

  it('renders page', () => {
    expect(dom).not.toBeNull();
  });

  describe('interval', () => {
    it('gets value from interval injectable', () => {
      const injectables: InjectablesMock = TestBed.get(InjectablesMock);
      expect(sut.interval).toBe(injectables.interval);
    });
  });

  describe('now', () => {
    it('gets value from getNow injectable', () => {
      const injectables: InjectablesMock = TestBed.get(InjectablesMock);
      expect(sut.now).toBe(injectables.getNow());
    });
  });
});
