import {ComponentFixture, TestBed} from '@angular/core/testing';
import {TestExamplesModule} from '@app/examples/TestExamplesModule';
import {CustomObject} from '@app/examples/model/CustomObject';
import {OutputAttributesPage} from '@app/examples/pages/output-attributes/OutputAttributesPage';

describe('OutputAttributesPage', () => {
  let fixture: ComponentFixture<OutputAttributesPage>;
  let sut: OutputAttributesPage;
  let dom: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestExamplesModule.forRoot()],
    });
    fixture = TestBed.createComponent(OutputAttributesPage);
    sut = fixture.componentInstance;
    dom = fixture.nativeElement;
  });

  it('creates instance of page', () => {
    expect(sut).not.toBeNull();
  });

  it('renders page', () => {
    expect(dom).not.toBeNull();
  });

  describe('onClick', () => {
    it('appends logs', () => {
      const expectedLogsLength: number = sut.logs.length + 1;
      sut.onClick(new MouseEvent('click'));
      expect(sut.logs.length).toBe(expectedLogsLength);
    });
  });

  describe('onMouseEnter', () => {
    it('appends logs', () => {
      const expectedLogsLength: number = sut.logs.length + 1;
      sut.onMouseEnter(new MouseEvent('click'));
      expect(sut.logs.length).toBe(expectedLogsLength);
    });
  });

  describe('onMouseOut', () => {
    it('appends logs', () => {
      const expectedLogsLength: number = sut.logs.length + 1;
      sut.onMouseOut(new MouseEvent('click'));
      expect(sut.logs.length).toBe(expectedLogsLength);
    });
  });

  describe('onFiz', () => {
    it('appends logs', () => {
      const expectedLogsLength: number = sut.logs.length + 1;
      sut.onFiz(new CustomObject('Test'));
      expect(sut.logs.length).toBe(expectedLogsLength);
    });
  });
});
