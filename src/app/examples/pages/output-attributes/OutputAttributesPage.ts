import {Component} from '@angular/core';
import {CustomObject} from '@app/examples/model/CustomObject';

@Component({
  selector: 'app-output-attributes-page',
  templateUrl: 'OutputAttributesPage.html'
})
export class OutputAttributesPage {
  public logs: string[] = [];

  public onClick(event: MouseEvent): void {
    this.logs.push('onClick');
  }

  public onMouseEnter(event: MouseEvent): void {
    this.logs.push('onMouseEnter');
  }

  public onMouseOut(event: MouseEvent): void {
    this.logs.push('onMouseOut');
  }

  public onFiz(customObject: CustomObject): void {
    this.logs.push(`onFiz - ${customObject.orderNumber}`);
  }
}
