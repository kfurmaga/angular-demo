import {ComponentFixture, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {TestExamplesModule} from '@app/examples/TestExamplesModule';
import {GuardsPage} from '@app/examples/pages/guards/GuardsPage';

describe('GuardsPage', () => {
  let fixture: ComponentFixture<GuardsPage>;
  let sut: GuardsPage;
  let dom: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestExamplesModule.forRoot(), RouterTestingModule],
    });
    fixture = TestBed.createComponent(GuardsPage);
    sut = fixture.componentInstance;
    dom = fixture.nativeElement;
  });

  it('creates instance of page', () => {
    expect(sut).not.toBeNull();
  });

  it('renders page', () => {
    expect(dom).not.toBeNull();
  });
});
