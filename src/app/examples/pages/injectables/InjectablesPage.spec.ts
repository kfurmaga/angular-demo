import {ComponentFixture, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {TestExamplesModule} from '@app/examples/TestExamplesModule';
import {InjectablesPage} from '@app/examples/pages/injectables/InjectablesPage';

describe('InjectablesPage', () => {
  let fixture: ComponentFixture<InjectablesPage>;
  let sut: InjectablesPage;
  let dom: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestExamplesModule.forRoot(), RouterTestingModule],
    });
    fixture = TestBed.createComponent(InjectablesPage);
    sut = fixture.componentInstance;
    dom = fixture.nativeElement;
  });

  it('creates instance of page', () => {
    expect(sut).not.toBeNull();
  });

  it('renders page', () => {
    expect(dom).not.toBeNull();
  });
});
