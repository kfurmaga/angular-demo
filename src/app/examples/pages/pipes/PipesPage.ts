import {Component} from '@angular/core';

@Component({
  selector: 'app-pipes-page',
  templateUrl: 'PipesPage.html'
})
export class PipesPage {
  public createdAt: Date = new Date();
}
