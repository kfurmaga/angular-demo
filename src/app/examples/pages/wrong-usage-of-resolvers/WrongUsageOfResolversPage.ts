import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ListItem} from '@app/examples/model/ListItem';
import {getListItem} from '@app/examples/selectors/data';
import {getItemId} from '@app/examples/selectors/params';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-wrong-usage-of-resolver-page',
  templateUrl: 'WrongUsageOfResolversPage.html'
})
export class WrongUsageOfResolversPage implements OnInit {
  public itemId: number;
  public title: string;
  public excerpt: string;

  public constructor(private readonly _activatedRoute: ActivatedRoute) {
  }

  public ngOnInit(): void {
    this.itemId = getItemId(this._activatedRoute.snapshot.params);
    const listItem: ListItem = getListItem(this._activatedRoute.snapshot.data);
    this.title = listItem.title;
    this.excerpt = listItem.excerpt;
  }
}
