import {ComponentFixture, TestBed} from '@angular/core/testing';
import {ActivatedRoute} from '@angular/router';
import {RouterTestingModule} from '@angular/router/testing';
import {TestExamplesModule} from '@app/examples/TestExamplesModule';
import {ListItem} from '@app/examples/model/ListItem';
import {WrongUsageOfResolversPage} from '@app/examples/pages/wrong-usage-of-resolvers/WrongUsageOfResolversPage';
import {MockActivatedRoute} from '@app/shared/testing/MockActivatedRoute';

describe('WrongUsageOfResolversPage', () => {
  const itemId: number = 1;
  const resolvedListItem: ListItem = new ListItem('resolved title', 'resolved excerpt');
  let fixture: ComponentFixture<WrongUsageOfResolversPage>;
  let sut: WrongUsageOfResolversPage;
  let dom: HTMLElement;
  let activatedRoute: MockActivatedRoute;

  beforeEach(() => {
    activatedRoute = new MockActivatedRoute({listItem: resolvedListItem}, {itemId});

    TestBed.configureTestingModule({
      imports: [TestExamplesModule.forRoot(), RouterTestingModule],
      providers: [{provide: ActivatedRoute, useValue: activatedRoute}],
    });
    fixture = TestBed.createComponent(WrongUsageOfResolversPage);
    sut = fixture.componentInstance;
    dom = fixture.nativeElement;

    sut.ngOnInit();
    fixture.detectChanges();
  });

  it('creates instance of page', () => {
    expect(sut).not.toBeNull();
  });

  it('renders page', () => {
    expect(dom).not.toBeNull();
  });

  describe('title', () => {
    it('initially contains resolvedListItem.title', () => {
      expect(sut.title).toBe(resolvedListItem.title);
    });

    // This test fails because example doesn't support updating activatedRoute data
    // it('updates when activatedRoute.data.listItem was changed', () => {
    //   const updatedListItem: ListItem = new ListItem('updated title', 'updated excerpt');
    //
    //   activatedRoute.updateData('listItem', updatedListItem);
    //
    //   expect(sut.title).toBe(updatedListItem.title);
    // });
  });

  describe('excerpt', () => {
    it('initially contains resolvedListItem.excerpt', () => {
      expect(sut.excerpt).toBe(resolvedListItem.excerpt);
    });

    // This test fails because example doesn't support updating activatedRoute data
    // it('updates when activatedRoute.data.listItem was changed', () => {
    //   const updatedListItem: ListItem = new ListItem('updated title', 'updated excerpt');
    //
    //   activatedRoute.updateData('listItem', updatedListItem);
    //
    //   expect(sut.title).toBe(updatedListItem.excerpt);
    // });
  });

  describe('itemId', () => {
    it('initially contains itemId', () => {
      expect(sut.itemId).toBe(itemId);
    });

    // This test fails because example doesn't support updating activatedRoute data
    // it('updates when activatedRoute.data.roar was changed', () => {
    //   const updatedItemId: number = 333;
    //
    //   activatedRoute.updateParam('itemId', updatedItemId);
    //
    //   expect(sut.itemId).toHaveBeenCalledWith(updatedItemId);
    // });
  });
});
