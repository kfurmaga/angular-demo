import {TestBed} from '@angular/core/testing';
import {ActivatedRoute, RouterStateSnapshot} from '@angular/router';
import {RouterTestingModule} from '@angular/router/testing';
import {TestExamplesModule} from '@app/examples/TestExamplesModule';
import {SimpleGuard} from '@app/examples/guards/SimpleGuard';
import {MockActivatedRoute} from '@app/shared/testing/MockActivatedRoute';
import createSpyObj = jasmine.createSpyObj;

describe('SimpleGuard', () => {
  let sut: SimpleGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestExamplesModule.forRoot(), RouterTestingModule],
    });
    sut = TestBed.get(SimpleGuard);
  });

  it('creates instance of gard', () => {
    expect(sut).not.toBeNull();
  });

  describe('canActivate', () => {
    let activatedRoute: MockActivatedRoute;
    let state: RouterStateSnapshot;

    beforeEach(() => {
      activatedRoute = new MockActivatedRoute({}, {}, {magicWord: null});
      state = createSpyObj<RouterStateSnapshot>('RouterStateSnapshot', ['toString']);
    });

    it('returns true when activatedRoute.queryParams contains proper magicWord', () => {
      activatedRoute.updateQueryParam('magicWord', SimpleGuard.MAGIC_WORD);
      const result: any = sut.canActivate(activatedRoute.snapshot, state);
      expect(result).toBe(true);
    });

    it('doesn\'t return true when activatedRoute.queryParams doesn\'t contains proper magicWord', () => {
      activatedRoute.updateQueryParam('magicWord', 'not magic word');
      const result: any = sut.canActivate(activatedRoute.snapshot, state);
      expect(result).not.toBe(true);
    });
  });
});
