import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {getMagicWord} from '@app/examples/selectors/queryParams';
import {Observable} from 'rxjs';

@Injectable()
export class SimpleGuard implements CanActivate {
  public static readonly MAGIC_WORD: string = 'please let me in';

  public constructor(private readonly _router: Router) {}

  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean | UrlTree {
    return getMagicWord(route.queryParams) === SimpleGuard.MAGIC_WORD
      ? true
      : this._router.createUrlTree(['/', 'examples', 'injectables']);
  }
}
