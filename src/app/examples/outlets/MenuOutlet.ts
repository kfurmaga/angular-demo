import {Component} from '@angular/core';

@Component({
  selector: 'app-menu-outlet',
  templateUrl: 'MenuOutlet.html',
})
export class MenuOutlet {}
