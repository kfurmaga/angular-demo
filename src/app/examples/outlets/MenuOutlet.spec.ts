import {ComponentFixture, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {TestExamplesModule} from '@app/examples/TestExamplesModule';
import {MenuOutlet} from '@app/examples/outlets/MenuOutlet';

describe('MenuOutlet', () => {
  let fixture: ComponentFixture<MenuOutlet>;
  let sut: MenuOutlet;
  let dom: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestExamplesModule.forRoot(), RouterTestingModule],
    });
    fixture = TestBed.createComponent(MenuOutlet);
    sut = fixture.componentInstance;
    dom = fixture.nativeElement;
  });

  it('creates instance of outlet', () => {
    expect(sut).not.toBeNull();
  });

  it('renders outlet', () => {
    expect(dom).not.toBeNull();
  });
});
